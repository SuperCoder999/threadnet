using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using Thread_.NET.BLL.Exceptions;

namespace Thread_.NET.BLL.AES
{
    public sealed class AESResetPasswordTokenGenerator
    {
        private static readonly byte[] Key = ToBytes(Environment.GetEnvironmentVariable("AESTextSecret") ?? "");
        private static readonly byte[] InitIV = ToBytes(Environment.GetEnvironmentVariable("AESInitializationIV") ?? "");
        private static readonly Regex validationRegex = new Regex(@"^([0-9a-z]+_){2}[0-9a-z]+$", RegexOptions.IgnoreCase);

        public string CreateResetPasswordToken(int userId, string userEmail)
        {
            byte[] IV = null;

            using (AesManaged generator = new AesManaged())
            {
                generator.GenerateIV();
                IV = generator.IV;
            }

            string part1 = Encrypt(userId.ToString(), IV);
            string part2 = Encrypt(userEmail, IV);
            string part3 = Encrypt(ToHex(IV), InitIV);

            return $"{part1}_{part2}_{part3}";
        }

        public void DecryptResetPasswordToken(string token, out int id, out string email)
        {
            if (!ValidateResetPasswordToken(token))
            {
                throw new InvalidEntityException("password reset token");
            }

            try
            {
                string[] parts = token.Split("_");

                byte[] IV = FromHexToBytes(Decrypt(parts[2], InitIV));
                id = Convert.ToInt32(Decrypt(parts[0], IV));
                email = Decrypt(parts[1], IV);
            }
            catch
            {
                throw new InvalidEntityException("password reset token");
            }
        }

        public bool ValidateResetPasswordToken(string token)
        {
            return validationRegex.IsMatch(token);
        }

        private string Encrypt(string data, byte[] IV)
        {
            using (AesManaged managed = new AesManaged())
            {
                managed.Mode = CipherMode.CBC;
                managed.KeySize = 256;
                managed.Key = Key;
                managed.IV = IV;

                return ClearEncrypt(data, managed);
            }
        }

        private string ClearEncrypt(string data, AesManaged managed)
        {
            byte[] encrypted;
            ICryptoTransform encryptor = managed.CreateEncryptor(managed.Key, managed.IV);

            using (MemoryStream memory = new MemoryStream())
            {
                using (CryptoStream crypto = new CryptoStream(memory, encryptor, CryptoStreamMode.Write))
                {
                    using (StreamWriter writer = new StreamWriter(crypto))
                    {
                        writer.Write(data);
                    }

                    encrypted = memory.ToArray();
                }
            }

            return ToHex(encrypted);
        }

        private string Decrypt(string chipher, byte[] IV)
        {
            using (AesManaged managed = new AesManaged())
            {
                managed.Mode = CipherMode.CBC;
                managed.KeySize = 256;
                managed.Key = Key;
                managed.IV = IV;

                return ClearDecrypt(chipher, managed);
            }
        }

        private string ClearDecrypt(string chipher, AesManaged managed)
        {
            ICryptoTransform decryptor = managed.CreateDecryptor(managed.Key, managed.IV);

            using (MemoryStream memory = new MemoryStream(FromHexToBytes(chipher)))
            {
                using (CryptoStream crypto = new CryptoStream(memory, decryptor, CryptoStreamMode.Read))
                {
                    using (StreamReader reader = new StreamReader(crypto, Encoding.UTF8))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
        }

        private static string ToHex(byte[] data)
        {
            return Convert.ToHexString(data);
        }

        private static byte[] ToBytes(string data)
        {
            return Encoding.UTF8.GetBytes(data);
        }

        private static byte[] FromHexToBytes(string hex)
        {
            return Convert.FromHexString(hex);
        }
    }
}