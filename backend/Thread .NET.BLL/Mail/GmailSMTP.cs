using System;
using System.Net;
using System.Net.Mail;
using System.Collections.Generic;

namespace Thread_.NET.BLL.Mail
{
    public sealed class GmailSMTP : ISMTP
    {
        private readonly SmtpClient client;
        private readonly MailAddress from;

        public GmailSMTP()
        {
            string user = Environment.GetEnvironmentVariable("GmailUser") ?? "";
            string password = Environment.GetEnvironmentVariable("GmailPassword") ?? "";
            from = new MailAddress(user, "Thread .NET");

            client = new SmtpClient("smtp.gmail.com")
            {
                Port = 587,
                EnableSsl = true,
                Credentials = new NetworkCredential(user, password),
            };
        }

        public void Send(string to, SubjectBodyPair data)
        {
            this.client.Send(GenerateMessage(to, data));
        }

        public void SendTemplate(
            string to,
            MailTemplate temp,
            IEnumerable<string> subjectParams,
            IEnumerable<string> bodyParams
        )
        {
            SubjectBodyPair data = temp.Parse(
                new List<string>(subjectParams),
                new List<string>(bodyParams)
            );

            this.Send(to, data);
        }

        public void SendTemplate(
            string to,
            MailTemplate temp,
            IEnumerable<string> bodyParams
        )
        {
            SubjectBodyPair data = temp.Parse(
                new List<string>(),
                new List<string>(bodyParams)
            );

            this.Send(to, data);
        }

        private MailMessage GenerateMessage(string to, SubjectBodyPair data)
        {
            MailMessage message = new MailMessage
            {
                From = from,
                Subject = data.Subject,
                Body = data.Body,
                IsBodyHtml = data.IsHTML,
            };

            message.To.Add(new MailAddress(to));
            return message;
        }
    }
}
