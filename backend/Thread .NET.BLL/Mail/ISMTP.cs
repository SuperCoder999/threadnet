using System.Collections.Generic;

namespace Thread_.NET.BLL.Mail
{
    public interface ISMTP
    {
        void Send(string to, SubjectBodyPair data);
        void SendTemplate(string to, MailTemplate template, IEnumerable<string> bodyParams);
        void SendTemplate(string to, MailTemplate template, IEnumerable<string> subjectParams, IEnumerable<string> bodyParams);
    }
}
