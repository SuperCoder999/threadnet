using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Thread_.NET.BLL.Mail
{
    public class MailTemplate : SubjectBodyPair
    {
        private static readonly Regex bracketsExp = new Regex(@"\{\}", RegexOptions.Multiline);
        private readonly int countInSubject;
        private readonly int countInBody;

        public MailTemplate(string subject, string body, bool isHTML = false) : base(subject, body, isHTML)
        {
            countInSubject = bracketsExp.Matches(Subject).Count;
            countInBody = bracketsExp.Matches(Body).Count;
        }

        public SubjectBodyPair Parse(List<string> subjectParams, List<string> bodyParams)
        {
            if (countInSubject > subjectParams.Count)
            {
                for (int i = 0; i < (countInSubject - subjectParams.Count); i++)
                {
                    subjectParams.Add("");
                }
            }

            if (countInBody > bodyParams.Count)
            {
                for (int i = 0; i < (countInBody - bodyParams.Count); i++)
                {
                    bodyParams.Add("");
                }
            }

            string subject = Subject.ToString();
            string body = Body.ToString();

            for (int i = 0; i < countInSubject; i++)
            {
                subject = bracketsExp.Replace(subject, subjectParams[i], 1);
            }

            for (int i = 0; i < countInBody; i++)
            {
                body = bracketsExp.Replace(body, bodyParams[i], 1);
            }

            return new SubjectBodyPair(subject, body, IsHTML);
        }
    }
}
