namespace Thread_.NET.BLL.Mail
{
    public static class TemplatesFactory
    {
        public static readonly MailTemplate PostLike = new MailTemplate(
            "Like!",
            @"Hello,<br />
Your post was liked by user {}!<br />
See liked post <a href='{}'>here</a>.",
            true
        );

        public static readonly MailTemplate PostShare = new MailTemplate(
            "Share!",
            @"Hello,<br />
User {} would like to share a post with you!<br />
To see it, go <a href='{}'>here</a>.",
            true
        );

        public static readonly MailTemplate ResetPassword = new MailTemplate(
            "Password reset",
            @"Hello,<br />
Password reset is requested for you.
<h3>If you didn't request this reset, just IGNORE THIS MESSAGE</h3>
To reset your password, use <a href='{}'>this link</a>.<br />
<b>NOTE:</b> link will expire in 2 hours.",
            true
        );
    }
}
