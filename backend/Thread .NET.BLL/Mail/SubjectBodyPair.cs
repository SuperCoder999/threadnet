namespace Thread_.NET.BLL.Mail
{
    public class SubjectBodyPair
    {
        public string Subject { get; }
        public string Body { get; }
        public bool IsHTML { get; }

        public SubjectBodyPair(string subject, string body, bool isHTML = false)
        {
            Subject = subject;
            Body = body;
            IsHTML = isHTML;
        }
    }
}
