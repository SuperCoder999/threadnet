﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.SignalR;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Reaction;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Mail;

namespace Thread_.NET.BLL.Services
{
    public sealed class PostReactionService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;
        private readonly ISMTP _smtp;
        private readonly PostService _postService;

        public PostReactionService(
            ThreadContext context,
            IMapper mapper,
            IHubContext<PostHub> postHub,
            ISMTP smtp,
            PostService postService
        ) : base(context, mapper)
        {
            _postHub = postHub;
            _smtp = smtp;
            _postService = postService;
        }

        public async Task ReactPost(NewReactionDTO reaction)
        {
            Post post = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Reactions)
                .Include(post => post.Comments.Where(c => !c.IsDeleted))
                    .ThenInclude(comment => comment.Author)
                .Where(post => post.Id == reaction.EntityId && !post.IsDeleted)
                .FirstOrDefaultAsync();

            if (post == null)
            {
                throw new NotFoundException("post", reaction.EntityId);
            }

            PostReaction currentReaction = null;
            bool removed = false;

            var currentReactionAsArray = _context.PostReactions.Where(r =>
                r.UserId == reaction.UserId &&
                r.PostId == reaction.EntityId);

            if (currentReactionAsArray.Any())
            {
                currentReaction = currentReactionAsArray.FirstOrDefault();
            }

            if (currentReaction == null)
            {
                _context.PostReactions.Add(new PostReaction
                {
                    PostId = reaction.EntityId,
                    IsLike = reaction.IsLike,
                    UserId = reaction.UserId
                });
            }
            else if (currentReaction.IsLike != reaction.IsLike)
            {
                _context.Update(currentReaction);
                currentReaction.IsLike = reaction.IsLike;
            }
            else
            {
                _context.RemoveRange(currentReactionAsArray);
                removed = true;
            }

            await _context.SaveChangesAsync();

            List<ReactionDTO> reactions = _context.PostReactions
                .Where(r => r.PostId == post.Id)
                .Include(r => r.User)
                .ToList()
                .ConvertAll<ReactionDTO>(r => _mapper.Map<ReactionDTO>(r));

            PostDTO updatedDto = _mapper.Map<PostDTO>(post);
            updatedDto.Reactions = (ICollection<ReactionDTO>)reactions;
            await _postHub.Clients.All.SendAsync("UpdatedPost", post.Id, updatedDto);

            User liker = await _context.Users
                .Where(u => u.Id == reaction.UserId)
                .FirstAsync();

            if (!removed && reaction.IsLike && reaction.UserId != post.AuthorId)
            {
                await _postHub.Clients.All.SendAsync("LikedPost", post.AuthorId);

                _smtp.SendTemplate(
                    post.Author.Email,
                    TemplatesFactory.PostLike,
                    new string[] { liker.UserName, _postService.JustGeneratePostLink(post.Id).Link }
                );
            }
        }
    }
}
