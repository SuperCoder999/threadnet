﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;
using Thread_.NET.BLL.Mail;

namespace Thread_.NET.BLL.Services
{
    public sealed class PostService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;
        private readonly ISMTP _smtp;

        public PostService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub, ISMTP smtp) : base(context, mapper)
        {
            _postHub = postHub;
            _smtp = smtp;
        }

        public async Task<ICollection<PostDTO>> GetAllPosts(int currentUserId, PostFilterDTO filter)
        {
            List<Post> posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments.Where(c => !c.IsDeleted))
                    .ThenInclude(comment => comment.Reactions)
                        .ThenInclude(react => react.User)
                .Include(post => post.Comments.Where(c => !c.IsDeleted))
                    .ThenInclude(comment => comment.Author)
                .Where(p =>
                    !p.IsDeleted &&
                    (
                        filter.IsOnlyMine == null
                        ? true
                        : (filter.IsOnlyMine == true && p.AuthorId == currentUserId) ||
                            (filter.IsOnlyMine == false && p.AuthorId != currentUserId)
                    ) &&
                    (
                        filter.IsOnlyLikedByMe == null
                        ? true
                        : (filter.IsOnlyLikedByMe == true
                            && _context.PostReactions
                                .Any(r => r.UserId == currentUserId && r.PostId == p.Id && r.IsLike))
                    )
                )
                .OrderByDescending(post => post.CreatedAt)
                .Skip((filter.Page - 1) * filter.PageSize)
                .Take(filter.PageSize)
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<PostDTO> GetOnePost(int id)
        {
            Post post = await _context.Posts
                .Where(p => p.Id == id && !p.IsDeleted)
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments.Where(c => !c.IsDeleted))
                    .ThenInclude(comment => comment.Reactions)
                        .ThenInclude(react => react.User)
                .Include(post => post.Comments.Where(c => !c.IsDeleted))
                    .ThenInclude(comment => comment.Author)
                .FirstOrDefaultAsync();

            if (post == null)
            {
                throw new NotFoundException("post", id);
            }

            return _mapper.Map<PostDTO>(post);
        }

        public async Task<PostLinkDTO> GetPostLink(int id)
        {
            try
            {
                await _context.Posts.Where(p => p.Id == id && !p.IsDeleted).FirstAsync();
            }
            catch
            {
                throw new NotFoundException("post", id);
            }

            return JustGeneratePostLink(id);
        }

        public PostLinkDTO JustGeneratePostLink(int id)
        {
            string frontLink = Environment.GetEnvironmentVariable("FrontendLink");

            return new PostLinkDTO
            {
                Link = $"{frontLink}/post/{id}",
            };
        }

        public async Task SharePostByEmail(int id, int userId, string email)
        {
            User user = await _context.Users
                .Where(u => u.Id == userId)
                .FirstAsync();

            _smtp.SendTemplate(
                email,
                TemplatesFactory.PostShare,
                new string[] { user.UserName, JustGeneratePostLink(id).Link }
            );
        }

        public async Task<PostDTO> CreatePost(PostCreateDTO postDto)
        {
            var postEntity = _mapper.Map<Post>(postDto);

            _context.Posts.Add(postEntity);
            await _context.SaveChangesAsync();

            var createdPost = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .FirstAsync(post => post.Id == postEntity.Id);

            var createdPostDTO = _mapper.Map<PostDTO>(createdPost);
            await _postHub.Clients.All.SendAsync("NewPost", createdPostDTO);

            return createdPostDTO;
        }

        public async Task<PostDTO> UpdatePost(int id, int userId, PostUpdateDTO data)
        {
            Post oldPost = await _context.Posts
                .Where(p => p.Id == id && p.AuthorId == userId && !p.IsDeleted)
                .FirstOrDefaultAsync();

            if (oldPost == null)
            {
                throw new NotFoundException("post", id);
            }

            if (oldPost.AuthorId != userId)
            {
                throw new AccessDeniedException("post", id);
            }

            Image image = await _context.Images
                .Where(i => i.URL == data.PreviewImage)
                .FirstOrDefaultAsync();

            _context.Posts.Update(oldPost);
            oldPost.Body = data.Body;
            oldPost.PreviewId = image != null ? image.Id : null;

            await _context.SaveChangesAsync();

            Post result = await _context.Posts
                .Where(p => p.Id == id)
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Reactions)
                .Include(post => post.Comments.Where(c => !c.IsDeleted))
                    .ThenInclude(comment => comment.Author)
                .FirstOrDefaultAsync();

            PostDTO resultDto = _mapper.Map<PostDTO>(result);
            await _postHub.Clients.All.SendAsync("UpdatedPost", id, resultDto);

            return resultDto;
        }

        public async Task DeletePost(int id, int userId)
        {
            Post post = await _context.Posts
                .Where(p => p.Id == id && !p.IsDeleted)
                .FirstOrDefaultAsync();

            if (post == null)
            {
                throw new NotFoundException("post", id);
            }

            if (post.AuthorId != userId)
            {
                throw new AccessDeniedException("post", id);
            }

            _context.Posts.Update(post);
            post.IsDeleted = true;

            await _context.SaveChangesAsync();
            await _postHub.Clients.All.SendAsync("DeletedPost", id);
        }

        private bool RightPost(Post post, PostFilterDTO filter, int currentUserId)
        {
            bool mineCond = filter.IsOnlyMine == null
                ? true
                : (filter.IsOnlyMine == true && post.AuthorId == currentUserId) ||
                    (filter.IsOnlyMine == false && post.AuthorId != currentUserId);

            bool likedCond = filter.IsOnlyLikedByMe == null
                ? true
                : (filter.IsOnlyLikedByMe == true
                    && _context.PostReactions
                        .Any(r => r.UserId == currentUserId && r.PostId == post.Id && r.IsLike));

            return !post.IsDeleted && mineCond;// && likedCond;
        }
    }
}
