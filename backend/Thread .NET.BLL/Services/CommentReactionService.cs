using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Reaction;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Hubs;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentReactionService : BaseService
    {
        private readonly IHubContext<CommentHub> _commentHub;

        public CommentReactionService(ThreadContext context, IMapper mapper, IHubContext<CommentHub> commentHub) : base(context, mapper)
        {
            _commentHub = commentHub;
        }

        public async Task ReactComment(NewReactionDTO reaction)
        {
            Comment comment = await _context.Comments
                .Include(c => c.Reactions)
                .Include(c => c.Author)
                    .ThenInclude(u => u.Avatar)
                .Where(c => c.Id == reaction.EntityId && !c.IsDeleted)
                .FirstOrDefaultAsync();

            if (comment == null)
            {
                throw new NotFoundException("comment", reaction.EntityId);
            }

            CommentReaction currentReaction = null;

            var currentReactionAsArray = _context.CommentReactions.Where(r =>
                r.UserId == reaction.UserId &&
                r.CommentId == reaction.EntityId);

            if (currentReactionAsArray.Any())
            {
                currentReaction = currentReactionAsArray.FirstOrDefault();
            }

            if (currentReaction == null)
            {
                _context.CommentReactions.Add(new CommentReaction
                {
                    CommentId = reaction.EntityId,
                    IsLike = reaction.IsLike,
                    UserId = reaction.UserId
                });
            }
            else if (currentReaction.IsLike != reaction.IsLike)
            {
                currentReaction.IsLike = reaction.IsLike;
            }
            else
            {
                _context.RemoveRange(currentReactionAsArray);
            }

            await _context.SaveChangesAsync();

            List<ReactionDTO> reactions = _context.CommentReactions
                .Where(r => r.CommentId == comment.Id)
                .Include(r => r.User)
                .ToList()
                .ConvertAll<ReactionDTO>(r => _mapper.Map<ReactionDTO>(r));

            CommentDTO updatedDto = _mapper.Map<CommentDTO>(comment);
            updatedDto.Reactions = (ICollection<ReactionDTO>)reactions;
            await _commentHub.Clients.All.SendAsync("UpdatedComment", comment.PostId, comment.Id, updatedDto);
        }
    }
}
