﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using System.Linq;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Hubs;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        private readonly IHubContext<CommentHub> _commentHub;

        public CommentService(ThreadContext context, IMapper mapper, IHubContext<CommentHub> commentHub) : base(context, mapper)
        {
            _commentHub = commentHub;
        }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            CommentDTO resultDto = _mapper.Map<CommentDTO>(createdComment);
            await _commentHub.Clients.All.SendAsync("NewComment", commentEntity.PostId, resultDto);

            return resultDto;
        }

        public async Task<CommentDTO> UpdateComment(int id, int userId, CommentUpdateDTO data)
        {
            Comment oldComment = await _context.Comments
                .Where(c => c.Id == id && !c.IsDeleted)
                .FirstOrDefaultAsync();

            if (oldComment == null)
            {
                throw new NotFoundException("comment", id);
            }

            if (oldComment.AuthorId != userId)
            {
                throw new AccessDeniedException("comment", id);
            }

            _context.Comments.Update(oldComment);
            oldComment.Body = data.Body;
            await _context.SaveChangesAsync();

            Comment result = await _context.Comments
                .Where(c => c.Id == id)
                .Include(c => c.Reactions)
                .Include(c => c.Author)
                    .ThenInclude(a => a.Avatar)
                .FirstOrDefaultAsync();

            CommentDTO resultDto = _mapper.Map<CommentDTO>(result);
            await _commentHub.Clients.All.SendAsync("UpdatedComment", result.PostId, result.Id, resultDto);

            return resultDto;
        }

        public async Task DeleteComment(int id, int userId)
        {
            Comment comment = await _context.Comments
                .Where(c => c.Id == id && !c.IsDeleted)
                .FirstOrDefaultAsync();

            if (comment == null)
            {
                throw new NotFoundException("comment", id);
            }

            if (comment.AuthorId != userId)
            {
                throw new AccessDeniedException("comment", id);
            }

            _context.Comments.Update(comment);
            comment.IsDeleted = true;

            await _context.SaveChangesAsync();
            await _commentHub.Clients.All.SendAsync("DeletedComment", comment.PostId, comment.Id);
        }
    }
}
