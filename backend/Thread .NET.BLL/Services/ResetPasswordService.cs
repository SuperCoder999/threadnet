using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Thread_.NET.BLL.AES;
using Thread_.NET.BLL.Mail;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;
using Thread_.NET.Common.DTO.User;
using Thread_.NET.Common.Security;

namespace Thread_.NET.BLL.Services
{
    public sealed class ResetPasswordService : BaseService
    {
        private readonly ISMTP _smtp;
        private readonly AESResetPasswordTokenGenerator _tokenGenerator;

        public ResetPasswordService(
            ThreadContext context,
            IMapper mapper,
            AESResetPasswordTokenGenerator tokenGenerator,
            ISMTP smtp
        ) : base(context, mapper)
        {
            _tokenGenerator = tokenGenerator;
            _smtp = smtp;
        }

        public async Task CheckToken(string token)
        {
            await CheckTokenAndReturn(token);
        }

        public async Task<UserDTO> ResetPassword(string token, string newPassword)
        {
            User user = await CheckTokenAndReturn(token);
            _context.Users.Update(user);

            byte[] salt = SecurityHelper.GetRandomBytes();
            user.Salt = Convert.ToBase64String(salt);
            user.Password = SecurityHelper.HashPassword(newPassword, salt);
            user.PasswordResetExpires = DateTime.UtcNow.AddHours(-1);

            await _context.SaveChangesAsync();
            return _mapper.Map<UserDTO>(user);
        }

        public async Task RequestResetPassword(string email)
        {
            string frontLink = Environment.GetEnvironmentVariable("FrontendLink") ?? "";
            string link = $"{frontLink}/reset-password/{await GenerateToken(email)}";

            _smtp.SendTemplate(email, TemplatesFactory.ResetPassword, new string[] { link });
        }

        private async Task<string> GenerateToken(string email)
        {
            User user = await _context.Users
                .Where(u => u.Email == email)
                .FirstOrDefaultAsync();

            if (user == null)
            {
                throw new NotFoundException("user");
            }

            _context.Users.Update(user);
            user.PasswordResetExpires = DateTime.UtcNow.AddHours(2);

            await _context.SaveChangesAsync();
            return _tokenGenerator.CreateResetPasswordToken(user.Id, user.Email);
        }

        private async Task<User> CheckTokenAndReturn(string token)
        {
            _tokenGenerator.DecryptResetPasswordToken(token, out int id, out string email);

            User user = await _context.Users
                .Where(u => u.Id == id)
                .FirstOrDefaultAsync();

            if (user == null || user.Email != email)
            {
                throw new NotFoundException("user");
            }

            if (user.PasswordResetExpires < DateTime.UtcNow)
            {
                throw new ExpiredPasswordResetTokenException();
            }

            return user;
        }
    }
}
