using System;

namespace Thread_.NET.BLL.Exceptions
{
    public sealed class AccessDeniedException : Exception
    {
        public AccessDeniedException(string name, int id)
            : base($"Access to entity {name} with id ({id}) was denied.")
        {
        }

        public AccessDeniedException(string name) : base($"Access to entity {name} was denied.") { }
    }
}
