using System;
using System.Collections.Generic;

namespace Thread_.NET.BLL.Exceptions
{
    public sealed class InvalidEntityException : Exception
    {
        public InvalidEntityException(string entityName) : base($"Invalid entity {entityName}.") { }

        public InvalidEntityException(string entityName, IEnumerable<string> errors)
            : base($"Invalid entity {entityName}: {string.Join(", ", errors)}.") { }
    }
}
