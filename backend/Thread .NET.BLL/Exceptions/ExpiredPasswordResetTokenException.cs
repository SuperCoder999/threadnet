using System;

namespace Thread_.NET.BLL.Exceptions
{
    public sealed class ExpiredPasswordResetTokenException : Exception
    {
        public ExpiredPasswordResetTokenException() : base("Reset password token expired.") { }
    }
}
