namespace Thread_.NET.Common.DTO.User
{
    public sealed class OnlyEmailDTO
    {
        public string Email { get; set; }
    }
}
