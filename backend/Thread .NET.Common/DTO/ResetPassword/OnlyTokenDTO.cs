namespace Thread_.NET.Common.DTO.PasswordReset
{
    public sealed class OnlyTokenDTO
    {
        public string Token { get; set; }
    }
}
