namespace Thread_.NET.Common.DTO.PasswordReset
{
    public sealed class ResetDataDTO
    {
        public string Password { get; set; }
        public string Token { get; set; }
    }
}
