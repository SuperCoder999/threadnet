namespace Thread_.NET.Common.DTO.Post
{
    public sealed class PostLinkDTO
    {
        public string Link { get; set; }
    }
}
