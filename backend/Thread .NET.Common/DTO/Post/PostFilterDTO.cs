#nullable enable

namespace Thread_.NET.Common.DTO.Post
{
    public sealed class PostFilterDTO
    {
        public int Page { get; set; }
        public int PageSize { get; set; }
        public bool? IsOnlyMine { get; set; }
        public bool? IsOnlyLikedByMe { get; set; }
    }
}
