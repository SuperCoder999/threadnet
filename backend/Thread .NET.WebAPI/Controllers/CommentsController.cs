﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.Common.DTO.Reaction;
using Thread_.NET.Extensions;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly CommentService _commentService;
        private readonly CommentReactionService _reactionService;

        public CommentsController(CommentService commentService, CommentReactionService reactionService)
        {
            _commentService = commentService;
            _reactionService = reactionService;
        }

        [HttpPost]
        public async Task<ActionResult<CommentDTO>> CreatePost([FromBody] NewCommentDTO comment)
        {
            comment.AuthorId = this.GetUserIdFromToken();
            return Ok(await _commentService.CreateComment(comment));
        }

        [HttpPost("react")]
        public async Task<IActionResult> ReactComment(NewReactionDTO reaction)
        {
            reaction.UserId = this.GetUserIdFromToken();
            await _reactionService.ReactComment(reaction);

            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<CommentDTO>> UpdateComment([FromRoute] int id, [FromBody] CommentUpdateDTO data)
        {
            int userId = this.GetUserIdFromToken();
            return Ok(await _commentService.UpdateComment(id, userId, data));
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeletePost([FromRoute] int id)
        {
            int userId = this.GetUserIdFromToken();
            await _commentService.DeleteComment(id, userId);

            return NoContent();
        }
    }
}