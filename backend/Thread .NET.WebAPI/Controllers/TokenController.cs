﻿using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.Auth;
using Thread_.NET.Extensions;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly AuthService _authService;
        private readonly IValidator<RefreshTokenDTO> _refreshValidator;
        private readonly IValidator<RevokeRefreshTokenDTO> _revokeValidator;

        public TokenController(
            AuthService authService,
            IValidator<RefreshTokenDTO> refreshValidator,
            IValidator<RevokeRefreshTokenDTO> revokeValidator
        )
        {
            _authService = authService;
            _refreshValidator = refreshValidator;
            _revokeValidator = revokeValidator;
        }

        [HttpPost("refresh")]
        [AllowAnonymous]
        public async Task<ActionResult<AccessTokenDTO>> Refresh([FromBody] RefreshTokenDTO dto)
        {
            await _refreshValidator.ValidateAndThrowSafeExceptionAsync(dto);

            return Ok(await _authService.RefreshToken(dto));
        }

        [HttpPost("revoke")]
        public async Task<IActionResult> RevokeRefreshToken([FromBody] RevokeRefreshTokenDTO dto)
        {
            await _revokeValidator.ValidateAndThrowSafeExceptionAsync(dto);

            var userId = this.GetUserIdFromToken();
            await _authService.RevokeRefreshToken(dto.RefreshToken, userId);

            return Ok();
        }
    }
}