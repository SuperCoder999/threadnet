using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using FluentValidation;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.User;
using Thread_.NET.Common.DTO.PasswordReset;
using Thread_.NET.Extensions;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/reset-password")]
    [ApiController]
    [AllowAnonymous]
    public class ResetPasswordController : ControllerBase
    {
        private readonly ResetPasswordService _service;
        private readonly IValidator<OnlyEmailDTO> _emailValidator;
        private readonly IValidator<OnlyTokenDTO> _tokenValidator;
        private readonly IValidator<ResetDataDTO> _resetValidator;

        public ResetPasswordController(
            ResetPasswordService service,
            IValidator<OnlyEmailDTO> emailValidator,
            IValidator<OnlyTokenDTO> tokenValidator,
            IValidator<ResetDataDTO> resetValidator
        )
        {
            _service = service;
            _emailValidator = emailValidator;
            _tokenValidator = tokenValidator;
            _resetValidator = resetValidator;
        }

        [HttpPost("request")]
        public async Task<ActionResult> RequestReset([FromBody] OnlyEmailDTO data)
        {
            await _emailValidator.ValidateAndThrowSafeExceptionAsync(data);
            await _service.RequestResetPassword(data.Email);

            return Ok();
        }

        [HttpPost("check")]
        public async Task<ActionResult> CheckToken([FromBody] OnlyTokenDTO data)
        {
            await _tokenValidator.ValidateAndThrowSafeExceptionAsync(data);
            await _service.CheckToken(data.Token);

            return Ok();
        }

        [HttpPut]
        public async Task<ActionResult<UserDTO>> ResetPassword([FromBody] ResetDataDTO data)
        {
            await _resetValidator.ValidateAndThrowSafeExceptionAsync(data);

            return Ok(await _service.ResetPassword(data.Token, data.Password));
        }
    }
}
