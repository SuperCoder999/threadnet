﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.Reaction;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.Common.DTO.User;
using Thread_.NET.Extensions;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly PostService _postService;
        private readonly PostReactionService _reactionService;

        public PostsController(PostService postService, PostReactionService reactionService)
        {
            _postService = postService;
            _reactionService = reactionService;
        }

        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<PostDTO>> GetOne([FromRoute] int id)
        {
            return Ok(await _postService.GetOnePost(id));
        }

        [HttpGet("link/{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<PostLinkDTO>> GetLink([FromRoute] int id)
        {
            return Ok(await _postService.GetPostLink(id));
        }

        [HttpPost]
        public async Task<ActionResult<PostDTO>> CreatePost([FromBody] PostCreateDTO dto)
        {
            dto.AuthorId = this.GetUserIdFromToken();

            return Ok(await _postService.CreatePost(dto));
        }

        [HttpPost("share/{id}")]
        public async Task<ActionResult> SharePost([FromRoute] int id, [FromBody] OnlyEmailDTO data)
        {
            await _postService.SharePostByEmail(id, this.GetUserIdFromToken(), data.Email);

            return Ok();
        }

        [HttpPost("paginated")]
        [AllowAnonymous]
        public async Task<ActionResult<ICollection<PostDTO>>> Paginated([FromBody] PostFilterDTO filter)
        {
            int userId;

            try
            {
                userId = this.GetUserIdFromToken();
            }
            catch
            {
                userId = -1;
            }

            return Ok(await _postService.GetAllPosts(userId, filter));
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<PostDTO>> UpdatePost([FromRoute] int id, [FromBody] PostUpdateDTO data)
        {
            int userId = this.GetUserIdFromToken();
            return Ok(await _postService.UpdatePost(id, userId, data));
        }

        [HttpPost("react")]
        public async Task<IActionResult> ReactPost(NewReactionDTO reaction)
        {
            reaction.UserId = this.GetUserIdFromToken();
            await _reactionService.ReactPost(reaction);

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeletePost([FromRoute] int id)
        {
            int userId = this.GetUserIdFromToken();
            await _postService.DeletePost(id, userId);

            return NoContent();
        }
    }
}