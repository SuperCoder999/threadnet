using FluentValidation;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;

namespace Thread_.NET.Extensions
{
    public static class ValidatorExtensions
    {
        public static async Task ValidateAndThrowSafeExceptionAsync<T>(this IValidator<T> self, T instance)
        {
            try
            {
                await self.ValidateAndThrowAsync(instance);
            }
            catch (ValidationException exc)
            {
                throw new InvalidEntityException(typeof(T).Name, from err in exc.Errors select err.ErrorMessage);
            }
        }
    }
}
