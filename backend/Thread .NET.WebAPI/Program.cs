﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using dotenv.net;

namespace Thread_.NET
{
    public class Program
    {
        public static void Main(string[] args)
        {
            DotEnv.Load(
                new DotEnvOptions(
                    ignoreExceptions: false,
                    envFilePaths: new string[] { "../.env" }
                )
            );

            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
