using FluentValidation;
using Thread_.NET.Common.DTO.User;

namespace Thread_.NET.Validators
{
    public sealed class OnlyEmailDTOValidator : AbstractValidator<OnlyEmailDTO>
    {
        public OnlyEmailDTOValidator()
        {
            RuleFor(dto => dto.Email)
                .EmailAddress()
                .WithMessage("Email must be and email address");
        }
    }
}
