﻿using FluentValidation;
using Thread_.NET.Common.DTO.User;

namespace Thread_.NET.Validators
{
    public sealed class UserLoginDTOValidator : AbstractValidator<UserLoginDTO>
    {
        public UserLoginDTOValidator()
        {
            RuleFor(u => u.Email)
                .NotNull()
                .WithMessage("Email must not be null");

            RuleFor(u => u.Password)
                .NotNull()
                .WithMessage("Password must not be null");
        }
    }
}
