﻿using FluentValidation;
using Thread_.NET.Common.DTO.User;

namespace Thread_.NET.Validators
{
    public sealed class UserRegisterDTOValidator : AbstractValidator<UserRegisterDTO>
    {
        public UserRegisterDTOValidator()
        {
            RuleFor(u => u.UserName)
                .NotEmpty()
                    .WithMessage("Username must not be empty")
                .Length(3, 20)
                    .WithMessage("Username should be from 3 to 20 characters");

            RuleFor(u => u.Email)
                .EmailAddress()
                .WithMessage("Email must be and email address");

            RuleFor(u => u.Password)
                .Length(4, 16)
                .WithMessage("Password must be from 4 to 16 characters");
        }
    }
}
