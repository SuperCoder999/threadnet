using FluentValidation;
using Thread_.NET.Common.DTO.PasswordReset;

namespace Thread_.NET.Validators
{
    public sealed class ResetDataDTOValidator : AbstractValidator<ResetDataDTO>
    {
        public ResetDataDTOValidator()
        {
            RuleFor(dto => dto.Password)
                .Length(4, 16)
                .WithMessage("Password must be from 4 to 16 symbols long");

            RuleFor(dto => dto.Token)
                .NotEmpty()
                .WithMessage("Token can't be empty");
        }
    }
}
