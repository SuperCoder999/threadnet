using FluentValidation;
using Thread_.NET.Common.DTO.PasswordReset;

namespace Thread_.NET.Validators
{
    public sealed class OnlyTokenDTOValidator : AbstractValidator<OnlyTokenDTO>
    {
        public OnlyTokenDTOValidator()
        {
            RuleFor(dto => dto.Token)
                .NotEmpty()
                .WithMessage("Token can't be empty");
        }
    }
}
