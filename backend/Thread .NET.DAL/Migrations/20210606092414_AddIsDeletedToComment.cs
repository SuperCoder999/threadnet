﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class AddIsDeletedToComment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Comments",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 1, 9, new DateTime(2021, 6, 6, 12, 24, 13, 702, DateTimeKind.Local).AddTicks(9420), false, new DateTime(2021, 6, 6, 12, 24, 13, 702, DateTimeKind.Local).AddTicks(9900), 18 },
                    { 18, 15, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(470), false, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(470), 17 },
                    { 17, 8, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(450), true, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(460), 5 },
                    { 15, 14, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(420), false, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(420), 6 },
                    { 14, 14, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(400), false, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(410), 20 },
                    { 13, 12, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(390), false, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(390), 18 },
                    { 12, 2, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(370), false, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(370), 2 },
                    { 11, 3, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(350), true, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(360), 16 },
                    { 19, 3, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(490), false, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(490), 20 },
                    { 10, 14, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(340), false, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(340), 5 },
                    { 8, 5, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(300), false, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(300), 16 },
                    { 7, 17, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(280), true, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(290), 2 },
                    { 6, 16, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(270), false, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(270), 16 },
                    { 5, 17, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(250), false, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(250), 10 },
                    { 4, 8, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(230), true, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(230), 10 },
                    { 3, 15, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(210), true, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(210), 13 },
                    { 2, 20, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(190), false, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(190), 18 },
                    { 9, 18, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(320), true, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(320), 9 },
                    { 20, 9, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(550), true, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(550), 10 },
                    { 16, 13, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(440), true, new DateTime(2021, 6, 6, 12, 24, 13, 703, DateTimeKind.Local).AddTicks(440), 8 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Quas sint a illo reiciendis.", new DateTime(2021, 6, 6, 12, 24, 13, 695, DateTimeKind.Local).AddTicks(9200), 5, new DateTime(2021, 6, 6, 12, 24, 13, 695, DateTimeKind.Local).AddTicks(9690) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Ad beatae sed saepe sapiente odio.", new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(40), 17, new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(40) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Id sequi temporibus ut rerum omnis porro laudantium sed sapiente.", new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(100), 10, new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(100) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Repellat esse asperiores nemo aut omnis et saepe.", new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(140), 7, new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(140) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Sit voluptates explicabo exercitationem aperiam qui distinctio vel.", new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(190), 4, new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(190) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Quia et veritatis dolorum sequi labore officia.", new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(230), 9, new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(230) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Asperiores officia vitae quia eveniet et sit officia.", new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(280), 2, new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(280) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Quo neque ab reiciendis nam quam et reprehenderit ullam.", new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(320), 13, new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(320) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Dignissimos exercitationem commodi dolore quos id.", new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(370), 7, new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(380) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Repudiandae et consectetur voluptatem perferendis.", new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(410), 3, new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(410) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Sit tempora tenetur dolores impedit veniam.", new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(460), 1, new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(460) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Quos qui sequi ab aut consequatur qui aspernatur.", new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(510), 17, new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(510) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Voluptate voluptas voluptatibus voluptatem possimus cupiditate molestiae.", new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(550), 18, new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(550) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Et dolore exercitationem et ut voluptatum est ea.", new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(590), 1, new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(590) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Ut nulla fugit aut ratione ut vel repudiandae autem modi.", new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(630), 7, new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(640) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Beatae quos et quidem autem amet ea.", new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(670), 3, new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(670) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Quo in eos ratione magni provident placeat id et illum.", new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(720), 4, new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(730) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Et sint est aliquam aut voluptas et et minima eveniet.", new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(770), 17, new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(770) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Incidunt aut voluptatibus unde est libero.", new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(800), 18, new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(810) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Incidunt quis quia soluta eum.", new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(840), 10, new DateTime(2021, 6, 6, 12, 24, 13, 696, DateTimeKind.Local).AddTicks(840) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(1660), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1111.jpg", new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(4910) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5240), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/94.jpg", new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5240) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5260), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1020.jpg", new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5260) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5280), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1160.jpg", new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5280) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5290), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1172.jpg", new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5290) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5310), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/579.jpg", new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5310) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5330), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/78.jpg", new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5330) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5340), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/318.jpg", new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5340) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5350), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/934.jpg", new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5360) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5370), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/298.jpg", new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5370) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5380), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1120.jpg", new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5380) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5390), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/347.jpg", new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5390) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5400), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/870.jpg", new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5410) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5420), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/777.jpg", new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5420) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5430), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1163.jpg", new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5430) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5440), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1134.jpg", new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5440) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5450), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/896.jpg", new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5460) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5470), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/982.jpg", new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5470) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5480), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1017.jpg", new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5480) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5490), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/252.jpg", new DateTime(2021, 6, 6, 12, 24, 13, 467, DateTimeKind.Local).AddTicks(5490) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(7760), "https://picsum.photos/640/480/?image=1061", new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8270) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8430), "https://picsum.photos/640/480/?image=298", new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8430) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8440), "https://picsum.photos/640/480/?image=624", new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8450) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8460), "https://picsum.photos/640/480/?image=10", new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8460) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8470), "https://picsum.photos/640/480/?image=375", new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8480) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8490), "https://picsum.photos/640/480/?image=1030", new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8490) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8510), "https://picsum.photos/640/480/?image=406", new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8510) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8520), "https://picsum.photos/640/480/?image=630", new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8530) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8540), "https://picsum.photos/640/480/?image=654", new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8540) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8550), "https://picsum.photos/640/480/?image=884", new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8550) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8560), "https://picsum.photos/640/480/?image=40", new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8570) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8580), "https://picsum.photos/640/480/?image=223", new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8580) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8590), "https://picsum.photos/640/480/?image=53", new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8590) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8600), "https://picsum.photos/640/480/?image=167", new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8610) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8620), "https://picsum.photos/640/480/?image=1032", new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8620) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8630), "https://picsum.photos/640/480/?image=173", new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8630) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8640), "https://picsum.photos/640/480/?image=1006", new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8640) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8660), "https://picsum.photos/640/480/?image=782", new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8660) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8670), "https://picsum.photos/640/480/?image=249", new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8670) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8690), "https://picsum.photos/640/480/?image=934", new DateTime(2021, 6, 6, 12, 24, 13, 469, DateTimeKind.Local).AddTicks(8690) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(8370), true, 8, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(8370), 18 },
                    { 1, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(6000), true, 20, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(6960), 12 },
                    { 19, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(8350), false, 8, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(8350), 3 },
                    { 18, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(8330), true, 7, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(8340), 2 }
                });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 17, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(8320), false, 17, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(8320), 3 },
                    { 16, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(8300), true, 15, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(8300), 7 },
                    { 14, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(8270), false, 5, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(8270), 19 },
                    { 13, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(8250), false, 20, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(8250), 20 },
                    { 12, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(8230), false, 8, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(8240), 17 },
                    { 11, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(8020), true, 1, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(8020), 20 },
                    { 15, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(8280), false, 16, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(8290), 9 },
                    { 9, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(7830), true, 12, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(7840), 3 },
                    { 8, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(7810), true, 20, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(7820), 4 },
                    { 7, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(7800), true, 16, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(7800), 16 },
                    { 6, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(7780), true, 14, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(7780), 7 },
                    { 10, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(8000), true, 3, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(8000), 7 },
                    { 5, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(7760), false, 14, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(7760), 12 },
                    { 4, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(7740), true, 2, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(7740), 19 },
                    { 3, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(7710), true, 19, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(7720), 17 },
                    { 2, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(7680), false, 8, new DateTime(2021, 6, 6, 12, 24, 13, 699, DateTimeKind.Local).AddTicks(7690), 11 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "Modi quia quibusdam ipsam et ut ea.", new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(970), 33, new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(1470) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Et molestiae doloremque labore expedita sunt iure aut aut voluptatem. Quia voluptatem vero laborum sint. Sed est est.", new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(4470), 36, new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(4480) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "Vero molestiae labore similique cum saepe nostrum.\nQuidem sed nam magnam in qui quo aperiam autem quos.\nMolestiae rem neque eum ea corrupti officia ut exercitationem voluptatem.", new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(5150), 23, new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(5160) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "Cumque et distinctio ratione perspiciatis et consequatur doloribus.", new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(5250), 27, new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(5260) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "Nam rem consequatur recusandae id quam pariatur laborum commodi et.", new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(5330), 34, new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(5330) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Asperiores asperiores nam. Ducimus enim excepturi officiis non illo rerum. Veritatis deserunt recusandae dolores et veritatis velit dolorem sed.", new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(5430), 22, new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(5430) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Quod autem harum aut.\nUt laudantium laboriosam autem repellat tempore asperiores.\nQuidem voluptatem mollitia debitis aut at deserunt temporibus sit molestiae.\nIpsa reiciendis totam aliquam aut eveniet harum vitae illum quia.\nEos et dolorem iusto et commodi quisquam.", new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(6920), 25, new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(6930) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Amet omnis tempore occaecati numquam deleniti qui odit.", new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(7020), 29, new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(7030) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "Quidem et quia. Deleniti commodi deserunt quia libero. Aut ex illo. Ut tempora et et quos rem. Autem eligendi culpa qui nobis sequi.", new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(7160), 28, new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(7170) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Ut omnis quibusdam illum. Ea id sunt accusamus. Sint quo nam dolorem nesciunt aperiam. Aut eum et blanditiis incidunt cum. Aut fuga repudiandae autem non labore laudantium repudiandae numquam. Vel nostrum est esse quasi impedit veritatis sint id.", new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(7330), 39, new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(7330) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 15, "Cupiditate quia at eum autem. Quod nemo dicta tenetur impedit provident dignissimos earum commodi et. Id dolores pariatur. Et eum vitae consequatur tempore. Ut et est error magni impedit et enim sunt. Quidem sed quis beatae quas quia.", new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(7480), new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(7480) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "Omnis aut necessitatibus.\nCorrupti quasi non expedita id est sit nihil.\nVelit soluta ipsam.", new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(7560), 28, new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(7560) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Ad et vel et quia assumenda nulla distinctio. Autem enim voluptate id maiores et rerum saepe. Quidem optio laborum aliquid laborum et fugiat numquam. Dolorem saepe nemo consequuntur porro ducimus ducimus minima qui molestias. Minima ullam dicta repellat quae in placeat. Suscipit numquam doloremque molestiae dolores.", new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(7730), 39, new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(7730) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, "inventore", new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(8120), 39, new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(8130) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "Saepe voluptatem assumenda vel dolore iste a quis impedit molestiae. Quis commodi laboriosam laborum et corporis. Accusamus ut aut distinctio.", new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(8240), 36, new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(8240) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "Voluptas iure ea explicabo maxime quis nostrum. Veritatis tempora libero sequi consectetur suscipit. Molestias aut ab.", new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(8320), 25, new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(8330) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "Et quam eveniet optio odit incidunt. Corporis error illo doloremque. Ut ut eum harum repudiandae quasi recusandae omnis. Quidem inventore cum sed fugiat sequi. Velit id et soluta qui rerum quod cum.", new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(8440), 37, new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(8450) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "ipsum", new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(8470), 37, new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(8470) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "aspernatur", new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(8490), 24, new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(8490) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "nesciunt", new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(8510), 26, new DateTime(2021, 6, 6, 12, 24, 13, 692, DateTimeKind.Local).AddTicks(8520) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2021, 6, 6, 12, 24, 13, 504, DateTimeKind.Local).AddTicks(3210), "Kelvin.Lockman5@hotmail.com", "9ElTwJcoMUdrExl1CgaQ+kDCnv3hjxfvEp5SQg9UDcw=", "L2222h5EbtFjGA3GbW4b95Redj1O+oH5jSCxTHtfXh0=", new DateTime(2021, 6, 6, 12, 24, 13, 504, DateTimeKind.Local).AddTicks(4550), "Heather38" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2021, 6, 6, 12, 24, 13, 513, DateTimeKind.Local).AddTicks(5740), "Velda.Sawayn18@yahoo.com", "agHmadjtsZZNib/blzx9yjLayJ8ZCJL112GywTUATWw=", "+IV4nBAa9CpDj1UOsRBQvdjFzubQVIQhemce1AM9uoE=", new DateTime(2021, 6, 6, 12, 24, 13, 513, DateTimeKind.Local).AddTicks(5750), "Kara_Osinski99" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 522, DateTimeKind.Local).AddTicks(5080), "Arno89@gmail.com", "0P5plsuxg/k1S8GeCBbF0qFWDGlGtjD/Pf1XPUBP9dE=", "bz6giaaqL2vq6/sADAzOhof4Ex918s6UaTjue8Q9mTY=", new DateTime(2021, 6, 6, 12, 24, 13, 522, DateTimeKind.Local).AddTicks(5080), "Emelia33" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2021, 6, 6, 12, 24, 13, 531, DateTimeKind.Local).AddTicks(5270), "Breanna_King@hotmail.com", "haMQvH4s2z7Sbi6JmojqGenMuEoEOzKPOxPkwCuMUlc=", "3VdPKde4UCrTw5V6hYyVn4ewatt/XZbpQtiM8IKNWEM=", new DateTime(2021, 6, 6, 12, 24, 13, 531, DateTimeKind.Local).AddTicks(5270), "Chet71" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2021, 6, 6, 12, 24, 13, 540, DateTimeKind.Local).AddTicks(4460), "Sarah.Halvorson@hotmail.com", "bBriQPuQyBHyLk/P4YFTC3vtFXvxet8DWzIMVax5ENw=", "NPzBn4AqmNLcU9oPM6GHgn4vxRhRolPo/JbWGuMAWYM=", new DateTime(2021, 6, 6, 12, 24, 13, 540, DateTimeKind.Local).AddTicks(4470), "Creola_Bashirian61" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2021, 6, 6, 12, 24, 13, 549, DateTimeKind.Local).AddTicks(5240), "Dina_Zboncak@yahoo.com", "cZYu9sEceC8BqoJMYHE4YMdd18UKcv2DKx1vOAbeyUE=", "KMmOOHyWsUWOHntQTbiwVDDo82LLTVec3bW81gYJOdw=", new DateTime(2021, 6, 6, 12, 24, 13, 549, DateTimeKind.Local).AddTicks(5240), "Eriberto_Lesch" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2021, 6, 6, 12, 24, 13, 558, DateTimeKind.Local).AddTicks(6110), "Brain.Schumm64@yahoo.com", "nvar/q7wH8SbLicZ+GiGZx1/X71MIS00CkquuSMcqog=", "U82FiL2uXONp7LsAppAbQMKRfAZBq/PO3aZZm0XFr64=", new DateTime(2021, 6, 6, 12, 24, 13, 558, DateTimeKind.Local).AddTicks(6110), "Petra.McDermott" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2021, 6, 6, 12, 24, 13, 567, DateTimeKind.Local).AddTicks(5050), "Arlene42@gmail.com", "w7TxfNjBmjQO2p3f0XuxFaKvphlAz2Aq1AFXFF+hasQ=", "+/HWRzaBjL5yPi037dPspepdEIXmcpyUzQObjW/fpOE=", new DateTime(2021, 6, 6, 12, 24, 13, 567, DateTimeKind.Local).AddTicks(5060), "Santos.Mueller47" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2021, 6, 6, 12, 24, 13, 576, DateTimeKind.Local).AddTicks(2720), "Logan90@yahoo.com", "js28xqaA8sMmgv2sJawE067HMOS/bzycqiuc1qkhoBs=", "LYMFz/kzp7nq5lPOLxqdt/IhboBxVqoGi5vKmww/AG8=", new DateTime(2021, 6, 6, 12, 24, 13, 576, DateTimeKind.Local).AddTicks(2720), "Ward77" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2021, 6, 6, 12, 24, 13, 585, DateTimeKind.Local).AddTicks(520), "Zetta75@hotmail.com", "M94hRlRctPTP1Xk944WQXetyKHD+9mvGUJA6foGk0fw=", "nataiTo3G/5gHFfAEtIhoikSDnv5hOTqOcmTvsqI57I=", new DateTime(2021, 6, 6, 12, 24, 13, 585, DateTimeKind.Local).AddTicks(520), "Candida.Conroy22" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2021, 6, 6, 12, 24, 13, 593, DateTimeKind.Local).AddTicks(8200), "Jorge86@yahoo.com", "I26RbEEj4gqD8D2/fstWLtM0M+Wws6wrmICRm9c11WQ=", "x7RHkIIiMjna0mn8BDMjNWtGgtKNHYLFN3ydDMuzNkY=", new DateTime(2021, 6, 6, 12, 24, 13, 593, DateTimeKind.Local).AddTicks(8210), "Lilly_Wiza82" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2021, 6, 6, 12, 24, 13, 602, DateTimeKind.Local).AddTicks(5460), "Preston.Kautzer12@gmail.com", "OuitXv0BEHT8QFpIu+4kSbb6pP62QRwi5t+Ol/d97Bs=", "XAXrkYJxOJ2Y8BuuYnY2OM5pfZ8qX2XDd/TV/7+UWNg=", new DateTime(2021, 6, 6, 12, 24, 13, 602, DateTimeKind.Local).AddTicks(5470), "Luisa_Gusikowski73" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2021, 6, 6, 12, 24, 13, 611, DateTimeKind.Local).AddTicks(2670), "Grayce.Swaniawski10@gmail.com", "cOCq8EjnB6kKg3ST1yEgb+KK9uN0PBH9gFYvaMomR1c=", "zBAlbOe5sU6DYyJC9NthR3808Mwq4ks49EDAy6RIg3A=", new DateTime(2021, 6, 6, 12, 24, 13, 611, DateTimeKind.Local).AddTicks(2680), "Marques2" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2021, 6, 6, 12, 24, 13, 619, DateTimeKind.Local).AddTicks(9930), "Rosalinda.Little14@gmail.com", "E08P7qXnB7Gm8XMA6gMRMGZqz/ny8Ojgl28s2Fu3uFs=", "RWXX4RipaEjhwLPqLHDA1IIPR2JbNeXl1EaBSaYMB78=", new DateTime(2021, 6, 6, 12, 24, 13, 619, DateTimeKind.Local).AddTicks(9930), "Muhammad.Beier" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2021, 6, 6, 12, 24, 13, 628, DateTimeKind.Local).AddTicks(7170), "Laverne87@gmail.com", "s6EIqQ9+/4Zao8oRYnpEiiSK3YCqhrvMC640gJoyonQ=", "GmZEpxiWe3Gfo2xIuWeiFtaF5ijf7XdyHm4Tu4DqZlU=", new DateTime(2021, 6, 6, 12, 24, 13, 628, DateTimeKind.Local).AddTicks(7170), "Antonio57" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2021, 6, 6, 12, 24, 13, 639, DateTimeKind.Local).AddTicks(4130), "Geoffrey6@hotmail.com", "iP5+38dFc4kCWgZvx5uu/cupS0IWmUojsPYoEtlSUmE=", "VPnPZY9Kb+FpY06+i0SzL09FcNiAoSou+bSqXyPhM24=", new DateTime(2021, 6, 6, 12, 24, 13, 639, DateTimeKind.Local).AddTicks(4130), "Deja57" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2021, 6, 6, 12, 24, 13, 648, DateTimeKind.Local).AddTicks(3640), "Vivienne69@hotmail.com", "A48QBH4ZZMGi2HpKtZLgSFN16f+H9IDJS3UvFw+MgIE=", "WiM6CJtlwuZQds9hI8Q4gb/jIcApuHFx+f2SaCAbNk0=", new DateTime(2021, 6, 6, 12, 24, 13, 648, DateTimeKind.Local).AddTicks(3650), "Abe10" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2021, 6, 6, 12, 24, 13, 657, DateTimeKind.Local).AddTicks(880), "Viola53@hotmail.com", "VlAmnTp37RPMd3J8K3q11chwpGaIvnMamkocQVq6ev0=", "ei0ZCCDOJvipg+EUYd01UUlpZlU77RugxUHlBnY2O+Y=", new DateTime(2021, 6, 6, 12, 24, 13, 657, DateTimeKind.Local).AddTicks(880), "Deontae21" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2021, 6, 6, 12, 24, 13, 668, DateTimeKind.Local).AddTicks(800), "Antonia.Dare@gmail.com", "FkSAtgwVGQKnI/YMfupUCI0qjSmuaCAw0DldmHY8a2g=", "uMT93LUKMpmSlAQo+GoqHEkv5AbmgQs4/Bbo4xaEY4c=", new DateTime(2021, 6, 6, 12, 24, 13, 668, DateTimeKind.Local).AddTicks(820), "Kenton_Aufderhar20" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 676, DateTimeKind.Local).AddTicks(7630), "Theodore28@hotmail.com", "uGuGBPFpM41LG7LoEqhYKPCQ9oCIZ0lqq2poUtinIUo=", "x5rRiwjx5dioKyHCMt2s2Gh0VaEliipuXdqbnRDFSj8=", new DateTime(2021, 6, 6, 12, 24, 13, 676, DateTimeKind.Local).AddTicks(7640), "Guadalupe.Prosacco19" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 6, 12, 24, 13, 685, DateTimeKind.Local).AddTicks(8190), "oeQg0z7GILzrRSg587npGku/8QfYe8JSeeEXbh9AhA4=", "qkF646Eq2ebHUhdIhoAmDoRcDQjOOhrUV0rF/2lK3vo=", new DateTime(2021, 6, 6, 12, 24, 13, 685, DateTimeKind.Local).AddTicks(8190) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Comments");

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 1, 9, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(4750), false, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5230), 13 },
                    { 18, 13, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5780), false, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5780), 20 },
                    { 17, 3, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5760), true, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5770), 18 },
                    { 15, 14, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5730), false, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5740), 3 },
                    { 14, 15, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5720), false, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5720), 3 },
                    { 13, 4, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5700), true, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5710), 2 },
                    { 12, 5, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5690), true, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5690), 5 },
                    { 11, 2, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5670), true, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5670), 2 },
                    { 19, 12, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5790), true, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5800), 4 },
                    { 10, 9, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5660), false, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5660), 17 },
                    { 8, 6, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5630), true, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5630), 1 },
                    { 7, 3, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5610), true, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5610), 7 },
                    { 6, 9, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5600), true, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5600), 10 },
                    { 5, 8, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5580), true, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5580), 18 },
                    { 4, 19, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5560), true, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5570), 4 },
                    { 3, 4, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5550), false, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5550), 4 },
                    { 2, 8, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5520), true, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5520), 6 },
                    { 9, 10, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5640), true, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5640), 14 },
                    { 20, 13, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5810), true, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5810), 21 },
                    { 16, 16, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5750), false, new DateTime(2021, 6, 5, 13, 17, 28, 834, DateTimeKind.Local).AddTicks(5750), 6 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Commodi qui consectetur odit et sed.", new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(7890), 8, new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(8370) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Reprehenderit asperiores et cumque neque dolore.", new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(8710), 2, new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(8710) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Dolores qui harum sed voluptatem corrupti.", new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(8760), 18, new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(8760) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Eligendi aut animi distinctio.", new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(8790), 2, new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(8790) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Placeat qui officiis necessitatibus hic.", new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(8840), 1, new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(8840) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Eligendi pariatur debitis qui.", new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(8880), 4, new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(8880) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Voluptas voluptatibus sunt ipsam ut possimus neque.", new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(8920), 15, new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(8920) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Reiciendis nemo minima vel debitis.", new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(8950), 20, new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(8950) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Commodi esse sint esse dolor corrupti aspernatur.", new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(8990), 10, new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(8990) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Rem laborum aut sed eaque nesciunt nobis.", new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(9030), 12, new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(9030) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Sed culpa officia laudantium ipsum dolorum ut dicta.", new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(9060), 16, new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(9070) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Sed voluptates amet ea omnis sit voluptates aut id.", new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(9110), 6, new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(9120) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Et autem sunt sequi eos.", new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(9150), 3, new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(9150) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Voluptatem nesciunt aliquam nihil repellendus eum ab et.", new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(9190), 11, new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(9190) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Sint nobis optio.", new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(9210), 15, new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(9220) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Porro voluptatem eos est vel ut dolores fugiat nostrum itaque.", new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(9260), 2, new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(9260) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Aut illum facilis sunt omnis sapiente.", new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(9290), 16, new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(9290) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Hic delectus libero dolore vitae quia consequatur.", new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(9330), 20, new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(9340) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Iure incidunt qui consequatur voluptatibus sint.", new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(9370), 4, new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(9370) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Officia quos necessitatibus vel neque explicabo velit adipisci ad.", new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(9410), 3, new DateTime(2021, 6, 5, 13, 17, 28, 827, DateTimeKind.Local).AddTicks(9410) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(4870), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/659.jpg", new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(7990) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8300), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/737.jpg", new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8310) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8330), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/5.jpg", new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8330) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8340), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/316.jpg", new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8340) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8350), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/583.jpg", new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8360) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8370), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/783.jpg", new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8370) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8390), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/876.jpg", new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8390) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8400), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1072.jpg", new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8410) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8420), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/928.jpg", new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8420) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8430), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1122.jpg", new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8430) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8440), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/195.jpg", new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8440) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8450), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/821.jpg", new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8460) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8470), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1090.jpg", new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8470) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8480), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/415.jpg", new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8480) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8490), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/442.jpg", new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8500) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8510), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1027.jpg", new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8510) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8520), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/500.jpg", new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8520) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8530), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/420.jpg", new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8530) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8540), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/936.jpg", new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8550) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8560), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/736.jpg", new DateTime(2021, 6, 5, 13, 17, 28, 602, DateTimeKind.Local).AddTicks(8560) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(300), "https://picsum.photos/640/480/?image=210", new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(820) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(980), "https://picsum.photos/640/480/?image=129", new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(980) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1000), "https://picsum.photos/640/480/?image=291", new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1000) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1010), "https://picsum.photos/640/480/?image=514", new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1010) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1020), "https://picsum.photos/640/480/?image=820", new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1030) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1040), "https://picsum.photos/640/480/?image=613", new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1040) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1060), "https://picsum.photos/640/480/?image=147", new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1060) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1070), "https://picsum.photos/640/480/?image=381", new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1070) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1090), "https://picsum.photos/640/480/?image=936", new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1090) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1100), "https://picsum.photos/640/480/?image=337", new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1100) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1110), "https://picsum.photos/640/480/?image=575", new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1110) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1120), "https://picsum.photos/640/480/?image=253", new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1130) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1140), "https://picsum.photos/640/480/?image=667", new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1140) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1150), "https://picsum.photos/640/480/?image=1066", new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1150) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1160), "https://picsum.photos/640/480/?image=693", new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1170) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1180), "https://picsum.photos/640/480/?image=815", new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1180) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1190), "https://picsum.photos/640/480/?image=376", new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1190) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1200), "https://picsum.photos/640/480/?image=714", new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1210) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1220), "https://picsum.photos/640/480/?image=88", new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1220) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1230), "https://picsum.photos/640/480/?image=730", new DateTime(2021, 6, 5, 13, 17, 28, 605, DateTimeKind.Local).AddTicks(1240) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3900), false, 6, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3900), 21 },
                    { 1, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(2780), true, 4, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3260), 1 },
                    { 19, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3890), true, 3, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3890), 13 },
                    { 18, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3870), true, 9, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3880), 13 }
                });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 17, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3860), true, 9, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3860), 19 },
                    { 16, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3840), false, 3, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3850), 20 },
                    { 14, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3810), false, 18, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3820), 18 },
                    { 13, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3800), true, 14, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3800), 1 },
                    { 12, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3780), false, 7, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3780), 2 },
                    { 11, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3710), true, 1, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3710), 2 },
                    { 15, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3830), true, 3, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3830), 10 },
                    { 9, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3680), true, 12, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3680), 2 },
                    { 8, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3660), false, 4, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3660), 19 },
                    { 7, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3640), false, 3, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3650), 4 },
                    { 6, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3630), true, 17, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3630), 21 },
                    { 10, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3690), false, 14, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3690), 4 },
                    { 5, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3610), false, 14, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3620), 5 },
                    { 4, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3600), true, 14, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3600), 10 },
                    { 3, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3580), true, 18, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3590), 12 },
                    { 2, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3560), true, 4, new DateTime(2021, 6, 5, 13, 17, 28, 831, DateTimeKind.Local).AddTicks(3560), 7 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "quasi", new DateTime(2021, 6, 5, 13, 17, 28, 821, DateTimeKind.Local).AddTicks(7530), 23, new DateTime(2021, 6, 5, 13, 17, 28, 821, DateTimeKind.Local).AddTicks(8090) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, "ut", new DateTime(2021, 6, 5, 13, 17, 28, 821, DateTimeKind.Local).AddTicks(8420), 24, new DateTime(2021, 6, 5, 13, 17, 28, 821, DateTimeKind.Local).AddTicks(8430) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "eos", new DateTime(2021, 6, 5, 13, 17, 28, 821, DateTimeKind.Local).AddTicks(8460), 22, new DateTime(2021, 6, 5, 13, 17, 28, 821, DateTimeKind.Local).AddTicks(8470) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "Placeat et veritatis ut animi maxime quis ut maiores. Sunt incidunt et. Dolore praesentium labore. Ad velit et omnis voluptatem quia dolores autem. Sit magnam molestiae.", new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(3640), 36, new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(3650) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, "Dolores et error magnam. Autem provident nesciunt optio qui libero non placeat. Cum dolorem cum cumque unde rerum maiores possimus id.", new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(3820), 37, new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(3820) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "Veritatis suscipit cumque vel aut.\nFacere mollitia tempore iure deserunt ea sit id qui dolor.\nEos ad nesciunt sunt aspernatur aspernatur.", new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(4450), 34, new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(4460) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "qui", new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(4500), 35, new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(4500) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Dignissimos et aspernatur commodi dignissimos nostrum illum voluptas odit vitae.\nQuis odit qui omnis.\nCorporis natus consequatur rerum odio consectetur.\nSapiente ut aut architecto et voluptas perferendis totam.", new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(4610), 39, new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(4610) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "Molestias sunt minus.\nQuae et illo quos.\nVel culpa neque eligendi possimus odio error commodi dolores cupiditate.\nCupiditate dolores consequuntur iusto numquam.\nBeatae explicabo laborum non laboriosam atque dolor laudantium debitis non.\nConsectetur ut aut dolores tempora perferendis nihil molestiae voluptates.", new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(6180), 24, new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(6190) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "Qui aspernatur impedit similique quis delectus ullam.\nSunt aliquid sapiente debitis id rem aspernatur maiores incidunt aut.\nQuod ipsum mollitia sint.\nExercitationem quia cumque.\nNemo repellendus architecto.\nQuo harum necessitatibus eum totam saepe quas.", new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(6370), 25, new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(6370) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 4, "consequatur", new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(6400), new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(6400) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Tenetur voluptas molestiae itaque quia.", new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(7060), 27, new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(7070) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Qui deserunt enim magnam ut nostrum expedita nemo minima.", new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(7130), 29, new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(7130) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "qui", new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(7160), 35, new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(7160) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Doloribus non itaque temporibus eos earum.", new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(7200), 39, new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(7200) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Nulla tenetur quia unde modi aperiam.\nDebitis dolorum aliquam aut ut in et vel minus.\nQuia consequatur et velit.\nMaxime ex hic rem et quisquam et.\nVoluptate occaecati et ad laudantium.", new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(7320), 27, new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(7320) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "Repudiandae rerum quo quia perferendis reiciendis commodi.\nAtque non possimus libero vel.\nUt et quia.\nAnimi facere ut beatae tempora exercitationem pariatur blanditiis dolorum.\nEaque illo laudantium sunt exercitationem.\nNihil est necessitatibus fuga nisi aut odit omnis labore.", new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(7470), 29, new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(7470) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Nemo modi sed ducimus sit quia culpa.\nEsse quam ad natus ex veritatis magnam molestiae maxime.", new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(7550), 24, new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(7550) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "Ex culpa voluptatum fuga.\nConsectetur quo repellendus quisquam aut enim et repellat.\nRecusandae sit omnis ipsum labore accusamus ea totam aliquid.", new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(7630), 36, new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(7630) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "non", new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(7650), 37, new DateTime(2021, 6, 5, 13, 17, 28, 824, DateTimeKind.Local).AddTicks(7660) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2021, 6, 5, 13, 17, 28, 640, DateTimeKind.Local).AddTicks(9090), "Elmer59@yahoo.com", "xeuNL+Btm2zyxxKdbkqim75idwCq6Ttgf3ZWVGEloA4=", "w3P0RFYaqq2NFHjl6gnq7WFlsI9GrawVwMOwxqaDVk0=", new DateTime(2021, 6, 5, 13, 17, 28, 640, DateTimeKind.Local).AddTicks(9630), "Karianne_Kreiger72" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2021, 6, 5, 13, 17, 28, 649, DateTimeKind.Local).AddTicks(9390), "Napoleon4@yahoo.com", "bHq4nr9sVdRecw6Y3iL9Gysq4DoYOSnGQx3ZbWitOk8=", "ZMxLC4FgxeLxJFsRXnxhDmtqPdVE6pJgOqY/1D+zRa4=", new DateTime(2021, 6, 5, 13, 17, 28, 649, DateTimeKind.Local).AddTicks(9400), "Theresa_Bauch60" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 658, DateTimeKind.Local).AddTicks(7820), "Esteban.Schmitt@yahoo.com", "FAhZIcL9TqXVwowpBL0Co5PVzX2n5ZCeNHgB94ABYZ4=", "ZRzRfROKVbtQ0Z7yhbK3C/mU1sKpIyr71izANCiHUFs=", new DateTime(2021, 6, 5, 13, 17, 28, 658, DateTimeKind.Local).AddTicks(7820), "Chelsie48" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2021, 6, 5, 13, 17, 28, 667, DateTimeKind.Local).AddTicks(5860), "Celestino.Okuneva@yahoo.com", "2RQ/dk4ck00hTVbpU5OT1IIJHLucBn/t9Oe+WHknQWU=", "wnQm1MnWWXee6x8NaRn36vTk40mSn1SQx41V4mqNS/I=", new DateTime(2021, 6, 5, 13, 17, 28, 667, DateTimeKind.Local).AddTicks(5860), "Domingo16" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2021, 6, 5, 13, 17, 28, 676, DateTimeKind.Local).AddTicks(3750), "Raheem.Lind@gmail.com", "h//e62inf729K2WN6XV2HKHfvrZFSG6S1hwJlM8kmRE=", "GkJtRp/iYZ3lJj4509iZmoCwY8U6w7EW1IQlulSoojs=", new DateTime(2021, 6, 5, 13, 17, 28, 676, DateTimeKind.Local).AddTicks(3760), "Jameson67" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2021, 6, 5, 13, 17, 28, 685, DateTimeKind.Local).AddTicks(1640), "Cleveland.Macejkovic49@yahoo.com", "lLHdh57MmSCl8lnwSc4OMU9OxSLOwtXBKf1cBNvBFkg=", "w1HGsUTZk5k64/rGkbWDGo39nROoZ0u99ciA/KuxkqY=", new DateTime(2021, 6, 5, 13, 17, 28, 685, DateTimeKind.Local).AddTicks(1650), "Arvel_Beatty" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2021, 6, 5, 13, 17, 28, 693, DateTimeKind.Local).AddTicks(9610), "Napoleon16@gmail.com", "CAZlsUYqdONtfC5OtSr6cQp10+UowTlLtQXEfyUEqmM=", "OL5r9d7przkCLdf+HcLAjGH04U05njmg5g2gPm3Wfws=", new DateTime(2021, 6, 5, 13, 17, 28, 693, DateTimeKind.Local).AddTicks(9610), "Nova.Toy20" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2021, 6, 5, 13, 17, 28, 702, DateTimeKind.Local).AddTicks(7500), "Haylee_Roberts51@gmail.com", "7Urxlfn3t9Lkj2iNcpcEVGK/ZDFqkCSrxkkl3cTDTc8=", "dzJWHAhyEvaXZ0HnRGlS1+cxCoXgRbEnKkYmbyPZ3AM=", new DateTime(2021, 6, 5, 13, 17, 28, 702, DateTimeKind.Local).AddTicks(7510), "Myah68" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2021, 6, 5, 13, 17, 28, 711, DateTimeKind.Local).AddTicks(6450), "Cordie.Wilderman@gmail.com", "GTAyqg69NxSm7cHaRZiglUQfciBdKkvjgdydY0WZNes=", "T1gbCvKRPPMKql46jLM38DxO7JywxKy7xiF9k5hhMuk=", new DateTime(2021, 6, 5, 13, 17, 28, 711, DateTimeKind.Local).AddTicks(6460), "Domenick24" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2021, 6, 5, 13, 17, 28, 720, DateTimeKind.Local).AddTicks(5450), "Dora.Lynch83@yahoo.com", "uUhEX0lnM1mq6Ss67/1bpDcoI4ODiOWInPKEgMnNWMk=", "Cow8lOwIvSautxIMRBwbSkIFhpTVm+AQZqVjcYSyT/M=", new DateTime(2021, 6, 5, 13, 17, 28, 720, DateTimeKind.Local).AddTicks(5460), "Jalyn_Gibson4" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2021, 6, 5, 13, 17, 28, 729, DateTimeKind.Local).AddTicks(5020), "Edwina88@gmail.com", "El4lnXb4z0BsjYgxE3tlB8zcJfmPAUYd07XmzB4c19E=", "Ujome0HqEA17Dy2FecIAG4LMxgRHBf9+rIj+pEHhKOw=", new DateTime(2021, 6, 5, 13, 17, 28, 729, DateTimeKind.Local).AddTicks(5030), "Kaelyn12" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2021, 6, 5, 13, 17, 28, 738, DateTimeKind.Local).AddTicks(2900), "Ofelia_Erdman36@yahoo.com", "96vRo1dt3mZSA/DTP4k6tr7C5uuOGwE/X39bHbGCYFw=", "xw2cdhSU0DGvtXTDncHoMsljXOv5RNaJogMzv94UTXw=", new DateTime(2021, 6, 5, 13, 17, 28, 738, DateTimeKind.Local).AddTicks(2900), "Camylle_McDermott" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2021, 6, 5, 13, 17, 28, 747, DateTimeKind.Local).AddTicks(2650), "Norwood77@gmail.com", "MOpMGgMxhMsbIO/sAGVFX11OTItS2IOZ5Ocpv5bTl34=", "26mXTkXA0en/ua+Iy1QuglxdseHaY++O2JKQZAx0f6M=", new DateTime(2021, 6, 5, 13, 17, 28, 747, DateTimeKind.Local).AddTicks(2660), "Monica.MacGyver95" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2021, 6, 5, 13, 17, 28, 756, DateTimeKind.Local).AddTicks(600), "Delores.Hermann@gmail.com", "uVtN9o1wnYB2iNkbhmdaViuCi7U7WnRMkYbboZynwsc=", "epJjVgU7SKDe9gqHfvwmVBQI/eLh8PBaisGpebij7cc=", new DateTime(2021, 6, 5, 13, 17, 28, 756, DateTimeKind.Local).AddTicks(600), "Chester.Schuster27" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2021, 6, 5, 13, 17, 28, 764, DateTimeKind.Local).AddTicks(8510), "Kyla68@gmail.com", "sGmBzNTih+/dUfeQAeaSoj4LvsY8DqTS8eXGIgwa+5Y=", "hChwxZF+RmMl5Np8jY9HjozIZLDulnFMIRMGnLhxWio=", new DateTime(2021, 6, 5, 13, 17, 28, 764, DateTimeKind.Local).AddTicks(8510), "Eldridge.Cremin50" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2021, 6, 5, 13, 17, 28, 773, DateTimeKind.Local).AddTicks(7050), "Marty_Tromp@hotmail.com", "uh94tOw/etUzFwP41Hcd9A7TE+60rIeTWZHCSgmrEJo=", "LpFSsyeyPIepK56BfQObNZPA1/EPXeH9v3XNM3KxZZA=", new DateTime(2021, 6, 5, 13, 17, 28, 773, DateTimeKind.Local).AddTicks(7060), "Ramon_Brakus20" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2021, 6, 5, 13, 17, 28, 782, DateTimeKind.Local).AddTicks(6120), "Alyce.Skiles17@gmail.com", "zC8JVd4uW3/YB0TFfjItKYx8ceKFjuUBUyAd6hjc0GM=", "P6Po+1ZaCHl2HWervT4hW4avF5dWMsIQLwJblDXqCNk=", new DateTime(2021, 6, 5, 13, 17, 28, 782, DateTimeKind.Local).AddTicks(6130), "Blaise24" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2021, 6, 5, 13, 17, 28, 791, DateTimeKind.Local).AddTicks(4150), "Marques.Bahringer97@hotmail.com", "gl/jYQpvjrNEqZswLHDa24ymQlVUYrlFBemZZ5asEWE=", "EiYYmqzzcfyVaiFNDrPyNwjbje2Q2Dzp2m5l2oUh44o=", new DateTime(2021, 6, 5, 13, 17, 28, 791, DateTimeKind.Local).AddTicks(4160), "Maymie_Klein31" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2021, 6, 5, 13, 17, 28, 800, DateTimeKind.Local).AddTicks(2070), "Adrianna.Pfannerstill70@gmail.com", "/+DB62soOybmVCWOV9sun4pXZ/8z3MCa+6eWhc37x3w=", "sQwAiFaIhJy04EbbCKAp7P5l12IA0fgjt3QSqFH0Mu0=", new DateTime(2021, 6, 5, 13, 17, 28, 800, DateTimeKind.Local).AddTicks(2070), "Kameron_Jerde99" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 809, DateTimeKind.Local).AddTicks(1650), "Jeanne_Prosacco@hotmail.com", "/lYXPVBwykNF8F2QNk0ztb6IZDGALue++LiBLMU7L8Q=", "nv3qVcZTduu4QLdjNg+QxCn+BPlIi2EYf00Y+rPjokE=", new DateTime(2021, 6, 5, 13, 17, 28, 809, DateTimeKind.Local).AddTicks(1650), "Ricky_Christiansen38" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 5, 13, 17, 28, 818, DateTimeKind.Local).AddTicks(1010), "zVsxsjbw97G8CJicUzQSIDlf3j5DRa8OW1R1MWas0EU=", "EjoopytgUouPDya/Czd8OmxYDWVxx9EgnzOttix9JxU=", new DateTime(2021, 6, 5, 13, 17, 28, 818, DateTimeKind.Local).AddTicks(1010) });
        }
    }
}
