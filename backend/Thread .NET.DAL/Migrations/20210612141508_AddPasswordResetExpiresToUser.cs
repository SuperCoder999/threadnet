﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class AddPasswordResetExpiresToUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DropColumn(
                name: "PasswordResetToken",
                table: "Users");

            migrationBuilder.AddColumn<DateTime>(
                name: "PasswordResetExpires",
                table: "Users",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 1, 8, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(7350), true, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(7840), 5 },
                    { 18, 14, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8490), true, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8500), 12 },
                    { 17, 2, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8470), false, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8480), 16 },
                    { 15, 10, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8440), true, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8440), 2 },
                    { 14, 14, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8420), false, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8420), 6 },
                    { 13, 7, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8400), false, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8400), 10 },
                    { 12, 4, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8380), false, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8380), 19 },
                    { 11, 18, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8320), true, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8320), 4 },
                    { 19, 16, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8510), true, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8510), 2 },
                    { 10, 18, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8300), true, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8300), 20 },
                    { 8, 8, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8260), false, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8270), 10 },
                    { 7, 19, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8250), false, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8250), 3 },
                    { 6, 20, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8230), false, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8230), 2 },
                    { 5, 10, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8210), false, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8210), 11 },
                    { 4, 6, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8190), false, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8190), 12 },
                    { 3, 1, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8170), true, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8170), 3 },
                    { 2, 19, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8130), true, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8130), 18 },
                    { 9, 10, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8280), false, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8280), 9 },
                    { 20, 9, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8530), true, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8530), 17 },
                    { 16, 17, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8460), true, new DateTime(2021, 6, 12, 17, 15, 8, 322, DateTimeKind.Local).AddTicks(8460), 14 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Quasi eos dolores.", new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(230), 7, new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(710) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Quis doloremque culpa sit.", new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1040), 9, new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1050) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 20, "Cupiditate veritatis porro eum corrupti doloremque.", new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1110), new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1110) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Odio velit dolores ut ut et quo.", new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1150), 8, new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1150) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Consequatur voluptatem asperiores temporibus officiis modi quas.", new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1200), 7, new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1200) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Enim voluptas est dolor numquam id magnam dolore.", new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1250), 14, new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1250) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Ea modi voluptatum quam ullam voluptatum dolor molestiae quam ipsam.", new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1300), 17, new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1300) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Quidem aut non ad nulla id voluptatibus dolore.", new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1350), 4, new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1350) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Cum unde tenetur voluptas et.", new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1410), 10, new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1410) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Est nobis ducimus explicabo occaecati consequatur in.", new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1450), 15, new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1460) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Aut consequatur assumenda sapiente delectus perspiciatis aut architecto reiciendis.", new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1500), 19, new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1500) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Rerum maxime illo sequi maxime placeat occaecati blanditiis.", new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1540), 17, new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1540) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Aut vitae repellat amet consequatur aut et enim vero.", new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1590), 8, new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1590) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Aut omnis porro qui voluptate eius omnis harum.", new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1630), 12, new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1640) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Ipsa nihil ipsa.", new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1670), 13, new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1670) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 3, "Tempora ut iste quasi sint quibusdam debitis delectus ea.", new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1710), new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1710) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Magni sunt velit.", new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1760), 1, new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1760) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Animi doloremque dolorem fugit.", new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1790), 15, new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1800) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Dolor reprehenderit doloremque tenetur.", new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1830), 20, new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1830) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Minima dignissimos et doloribus quo nam quis facere.", new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1870), 13, new DateTime(2021, 6, 12, 17, 15, 8, 316, DateTimeKind.Local).AddTicks(1880) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(2180), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1198.jpg", new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5400) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5730), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/980.jpg", new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5740) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5760), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/771.jpg", new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5760) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5770), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1066.jpg", new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5770) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5780), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/219.jpg", new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5790) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5800), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/702.jpg", new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5800) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5810), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/652.jpg", new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5810) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5820), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/74.jpg", new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5820) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5830), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/432.jpg", new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5840) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5850), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/700.jpg", new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5850) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5860), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/552.jpg", new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5860) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5870), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/750.jpg", new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5870) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5880), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1159.jpg", new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5890) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5900), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1203.jpg", new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5900) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5910), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1079.jpg", new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5910) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5920), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/302.jpg", new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5920) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5930), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/815.jpg", new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5940) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5940), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/174.jpg", new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5950) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5960), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/431.jpg", new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5960) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5970), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/575.jpg", new DateTime(2021, 6, 12, 17, 15, 8, 79, DateTimeKind.Local).AddTicks(5970) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(8540), "https://picsum.photos/640/480/?image=866", new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9030) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9180), "https://picsum.photos/640/480/?image=822", new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9190) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9200), "https://picsum.photos/640/480/?image=637", new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9200) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9220), "https://picsum.photos/640/480/?image=220", new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9230) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9240), "https://picsum.photos/640/480/?image=996", new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9240) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9250), "https://picsum.photos/640/480/?image=255", new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9250) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9270), "https://picsum.photos/640/480/?image=823", new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9270) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9280), "https://picsum.photos/640/480/?image=998", new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9280) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9290), "https://picsum.photos/640/480/?image=815", new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9290) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9310), "https://picsum.photos/640/480/?image=241", new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9310) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9320), "https://picsum.photos/640/480/?image=687", new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9320) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9330), "https://picsum.photos/640/480/?image=65", new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9330) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9340), "https://picsum.photos/640/480/?image=584", new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9350) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9360), "https://picsum.photos/640/480/?image=364", new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9360) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9370), "https://picsum.photos/640/480/?image=649", new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9370) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9380), "https://picsum.photos/640/480/?image=340", new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9390) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9400), "https://picsum.photos/640/480/?image=310", new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9410) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9420), "https://picsum.photos/640/480/?image=794", new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9420) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9430), "https://picsum.photos/640/480/?image=835", new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9430) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9440), "https://picsum.photos/640/480/?image=760", new DateTime(2021, 6, 12, 17, 15, 8, 81, DateTimeKind.Local).AddTicks(9450) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6660), true, 5, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6660), 17 },
                    { 1, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(5480), true, 10, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(5970), 3 },
                    { 19, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6640), false, 20, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6640), 4 },
                    { 18, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6620), false, 20, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6630), 8 }
                });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 17, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6600), false, 16, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6610), 1 },
                    { 16, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6570), false, 11, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6570), 17 },
                    { 14, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6530), false, 4, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6540), 9 },
                    { 13, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6520), true, 17, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6520), 13 },
                    { 12, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6500), false, 3, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6500), 2 },
                    { 11, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6480), false, 4, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6490), 18 },
                    { 15, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6550), false, 18, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6550), 4 },
                    { 9, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6450), false, 10, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6450), 15 },
                    { 8, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6430), false, 9, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6430), 3 },
                    { 7, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6410), true, 1, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6410), 18 },
                    { 6, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6340), false, 15, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6340), 19 },
                    { 10, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6460), true, 7, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6470), 9 },
                    { 5, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6310), false, 1, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6320), 3 },
                    { 4, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6300), false, 12, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6300), 16 },
                    { 3, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6280), false, 14, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6280), 17 },
                    { 2, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6250), false, 10, new DateTime(2021, 6, 12, 17, 15, 8, 319, DateTimeKind.Local).AddTicks(6250), 20 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "Velit sunt nesciunt.", new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(4040), 21, new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(4520) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "porro", new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(5160), 33, new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(5180) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Ut nam minima quis non corrupti et.\nVoluptatem quo pariatur enim officia deleniti nobis eius modi.\nQuo et magnam animi est.\nDolor qui numquam at.", new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(7600), 32, new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(7610) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 21, "Eligendi quisquam ut et debitis quam id dolore.\nDicta ea et necessitatibus praesentium consequatur.\nEt velit minus accusamus odio quis.", new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(7810), new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(7820) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 6, "molestias", new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(7860), new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(7860) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Et veniam sunt unde.", new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(7910), 38, new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(7910) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "Ut et reiciendis illum facere sint sit.", new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(7960), 39, new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(7970) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Inventore maxime qui ea quis ea. Voluptatem ut quis quis laudantium vero voluptatem eius beatae. Nisi error illo doloribus voluptates et consequatur saepe. Sed quam facilis eos et velit et.", new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(8950), 40, new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(8960) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "praesentium", new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(9040), 24, new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(9040) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Amet ab aut quidem excepturi sapiente quos ullam aliquam minima.", new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(9100), 31, new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(9100) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "Ratione cum aut excepturi nam et.", new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(9150), 28, new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(9150) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, "Sunt mollitia aut dicta explicabo et dolores vitae quasi.", new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(9200), 22, new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(9200) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Vel distinctio ipsa quia. Eos et eum excepturi unde doloremque. Ducimus distinctio mollitia fuga laborum similique.", new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(9290), 33, new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(9290) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Voluptate quia quidem.\nTotam ullam ad fuga quis officia quo magni alias.\nLabore incidunt ut est quaerat vitae suscipit inventore est.\nQuo atque dignissimos laudantium.\nNon accusamus et laudantium.\nQuidem sint placeat beatae animi non labore.", new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(9460), 39, new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(9460) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Deserunt et quis laboriosam aut provident quis. Quod aliquid iste rem rerum voluptatibus at vel reprehenderit. Quisquam non libero. Amet quia deleniti laudantium sunt qui. Esse ipsam autem iste enim saepe facilis sunt eveniet.", new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(9610), 23, new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(9620) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "Voluptas quis ea minima dolores et cum molestiae.", new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(9660), 31, new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(9670) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "in", new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(9690), 38, new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(9690) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 9, "Est dignissimos iste maiores voluptas aperiam omnis repudiandae harum.\nNon quisquam dolores.", new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(9750), new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(9760) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "Ut delectus velit sit rem illo quam. Facilis expedita sequi blanditiis quisquam doloremque ipsa eaque blanditiis necessitatibus. Facilis excepturi magnam mollitia consequatur et non totam nihil.", new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(9860), 21, new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(9860) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Deserunt quo vitae distinctio ipsam aut quas similique dignissimos. Aut possimus dolor consequatur sit cum. Sint quos dicta qui et est. Fugiat eum earum qui quo. Hic et fuga quidem magni ipsam sapiente vel eos.", new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(9990), 39, new DateTime(2021, 6, 12, 17, 15, 8, 312, DateTimeKind.Local).AddTicks(9990) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2021, 6, 12, 17, 15, 8, 118, DateTimeKind.Local).AddTicks(5420), "Maximus20@gmail.com", "gEvg+4EgCLVCVhWVNjhTfPKpO8tX/2txc8fJO7aBEwk=", "zNwEVJvXCOCQLCzQrIMqmd9grWEl3ApfMwVkZ7/fjso=", new DateTime(2021, 6, 12, 17, 15, 8, 118, DateTimeKind.Local).AddTicks(5940), "Adrian_Orn" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2021, 6, 12, 17, 15, 8, 128, DateTimeKind.Local).AddTicks(200), "Frida.Nolan@hotmail.com", "JpD64JnBO/+wfs538As2tW8DtLmblBRyDcBOKDgc3ys=", "cSYYi0jtx5UnMGZKmr/Z+XgHceRK5nww4OxK0CxV+dw=", new DateTime(2021, 6, 12, 17, 15, 8, 128, DateTimeKind.Local).AddTicks(220), "Larissa_Hickle" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2021, 6, 12, 17, 15, 8, 136, DateTimeKind.Local).AddTicks(8530), "Korey_Stroman@yahoo.com", "bnlrRUbGgNAzF51f4qObYSlehwHf2+uDJ/gLEyeekBY=", "vqpdteYid6gf6yBgFCPM0Ndo9SIecp3J2qZ/LnYLqgI=", new DateTime(2021, 6, 12, 17, 15, 8, 136, DateTimeKind.Local).AddTicks(8540), "Richmond.Treutel" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2021, 6, 12, 17, 15, 8, 145, DateTimeKind.Local).AddTicks(5980), "Kaley77@gmail.com", "ziCz1ACXJQwVZ8Z5ql7ZY72fSQNYdD2IvGKWF6JtpNI=", "tYnz5WlJjkjNSE0tMxZXUAQUCEFiQTJ+/eQJSudIgIc=", new DateTime(2021, 6, 12, 17, 15, 8, 145, DateTimeKind.Local).AddTicks(5980), "Amani.Wintheiser" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2021, 6, 12, 17, 15, 8, 154, DateTimeKind.Local).AddTicks(3390), "Daija_Veum@hotmail.com", "zZhra7OgOebNiY76e8ySzQXMctpVoD4OGtkYJ7kDLuo=", "D/aLRq8WvD0bwEytKxjnLQ4d+q0C5XSXMEek2x8t2FA=", new DateTime(2021, 6, 12, 17, 15, 8, 154, DateTimeKind.Local).AddTicks(3400), "Lowell50" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2021, 6, 12, 17, 15, 8, 165, DateTimeKind.Local).AddTicks(1750), "Thelma_OKeefe45@yahoo.com", "lk5viIbw4VPj8hSWSmbAjtyvklwKm+NDt5f41DDEphg=", "MzWn47mkuSt9APFmyqS/xK/WRiAz8Sm5Pcp9KZH8SeA=", new DateTime(2021, 6, 12, 17, 15, 8, 165, DateTimeKind.Local).AddTicks(1770), "Marco.Bauch" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2021, 6, 12, 17, 15, 8, 173, DateTimeKind.Local).AddTicks(9050), "Newton.Kulas75@yahoo.com", "43VZDUodO0ku145A94NkeeXOEKjqDPDWWYB3UJi//t0=", "iQHy4kWKECtCuMjSIeC5e+BDXGX2ewLAq+hCx9Xxh4U=", new DateTime(2021, 6, 12, 17, 15, 8, 173, DateTimeKind.Local).AddTicks(9060), "Era_Deckow96" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 182, DateTimeKind.Local).AddTicks(7830), "Leora.Nader@gmail.com", "EI2cq3rd0gVg2i5F6B0lZqiG5CMqXGEgsn79zO8092Q=", "kczzo4T99n49gPbYH7SVPtsQnkMDf5RY2WmkZ5gQLU8=", new DateTime(2021, 6, 12, 17, 15, 8, 182, DateTimeKind.Local).AddTicks(7840), "Mackenzie.Nienow51" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2021, 6, 12, 17, 15, 8, 191, DateTimeKind.Local).AddTicks(6450), "Brown.Russel@yahoo.com", "OdEr3dJfx/eRP0mEVk+/gw/eabngBg+yfxfbOoSloUk=", "xxk2uqHlVZDJIs/Nf02QxPNoDfpDNo6s0bRTtREvh8U=", new DateTime(2021, 6, 12, 17, 15, 8, 191, DateTimeKind.Local).AddTicks(6460), "Cara.Homenick" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2021, 6, 12, 17, 15, 8, 200, DateTimeKind.Local).AddTicks(5920), "Ricardo_Dicki60@gmail.com", "YQlfhszg0yeiz55xeTmir5U/XXCOGWLbUWZfPRrDIzM=", "6Qm/2AJmU+3JIDoX0ZNsqih5D2ywlrbxvCRR7HXbRPg=", new DateTime(2021, 6, 12, 17, 15, 8, 200, DateTimeKind.Local).AddTicks(5930), "Everette_Howe" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 209, DateTimeKind.Local).AddTicks(5000), "Mathias23@yahoo.com", "Kn5DI4ULOr5/4HZkKOfTI5PbA0KZ1KbViFk35yUr//c=", "XjrPJ9wsA8eNROQSCtt9B7kZc+bMAf70f1t/xjSrwUg=", new DateTime(2021, 6, 12, 17, 15, 8, 209, DateTimeKind.Local).AddTicks(5010), "Eloisa69" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2021, 6, 12, 17, 15, 8, 218, DateTimeKind.Local).AddTicks(3990), "Milan82@gmail.com", "PHMIcLomdS0jLpBFlpTbuxus1NSJjXsSVMSY8wjSs7k=", "WsJEbNbk7H9YH7OmhNVjJcLPM5GwBtQx71PEoopMKAc=", new DateTime(2021, 6, 12, 17, 15, 8, 218, DateTimeKind.Local).AddTicks(4010), "Davon_Block65" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2021, 6, 12, 17, 15, 8, 227, DateTimeKind.Local).AddTicks(4110), "Autumn.Wilderman74@gmail.com", "7vmjzE8hts7zsWbOdfRdgr989HZpmNecniCFHaRBGo4=", "CQzq5vQvsK9uprky8DJk3K7BH0xYo2sDabCy4OkZaLE=", new DateTime(2021, 6, 12, 17, 15, 8, 227, DateTimeKind.Local).AddTicks(4120), "Furman89" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2021, 6, 12, 17, 15, 8, 236, DateTimeKind.Local).AddTicks(3560), "Barrett_Dach76@yahoo.com", "wc1WbuPiI8IVMzXjU1OfxsbxQ7gVoV35SisWvItvTtY=", "tm2klMcydXWiwNC9stznCIWOV3NZlG5plH3skfKgcWY=", new DateTime(2021, 6, 12, 17, 15, 8, 236, DateTimeKind.Local).AddTicks(3570), "Clyde92" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2021, 6, 12, 17, 15, 8, 245, DateTimeKind.Local).AddTicks(1360), "Tabitha.Hagenes97@gmail.com", "6xzUJ+tDB4KpAHHDy6PCJ+hu8C63bYmOb7LGjqaMnMA=", "b7GkcRVoBHxu6nq6eZAA0PvA1fwxw34M5twXVqz6yvQ=", new DateTime(2021, 6, 12, 17, 15, 8, 245, DateTimeKind.Local).AddTicks(1360), "Gwendolyn.Heathcote77" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2021, 6, 12, 17, 15, 8, 253, DateTimeKind.Local).AddTicks(8700), "Kyle.Purdy89@yahoo.com", "xpnG5j4AuLuGq6dVZhQ8aXpoTEqBsijCkiYK0r0XdRA=", "PPghv9iqrbO1C4XOdcUZ1ZwAiWeRUEaUlAJ73JScOv8=", new DateTime(2021, 6, 12, 17, 15, 8, 253, DateTimeKind.Local).AddTicks(8710), "Alf.Brown" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2021, 6, 12, 17, 15, 8, 262, DateTimeKind.Local).AddTicks(6030), "Brennan_Stracke@yahoo.com", "Vr/yBIzbV3mBKseg0W1M4dUOP9gVm3Va2lO2gA90t40=", "UtI76mH4Ey+qtuipdcyIfABHPkrCDT4H2PnszS3Rufs=", new DateTime(2021, 6, 12, 17, 15, 8, 262, DateTimeKind.Local).AddTicks(6040), "Daija_Dickinson55" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2021, 6, 12, 17, 15, 8, 272, DateTimeKind.Local).AddTicks(6710), "Orlo26@yahoo.com", "tzQR484kCbHZr11xQpjuPybNt5ZkUPzMom7Kecoq8r8=", "ba0D2y42M1rhN7JNDT38FCmVFRT5m8VzDipTKz2Yxd8=", new DateTime(2021, 6, 12, 17, 15, 8, 272, DateTimeKind.Local).AddTicks(6730), "Paula3" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2021, 6, 12, 17, 15, 8, 286, DateTimeKind.Local).AddTicks(1080), "Samantha_Balistreri2@yahoo.com", "oXaf/8+0Tqc+E1UGvR5akQC3u0HPAaKogu3V/hpiR14=", "BQyTHnXNHLBI1RNI/mjZkkdcd2d1lRmVMRYEsSJ9rPc=", new DateTime(2021, 6, 12, 17, 15, 8, 286, DateTimeKind.Local).AddTicks(1110), "Forest_Von" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2021, 6, 12, 17, 15, 8, 296, DateTimeKind.Local).AddTicks(6560), "Kaylah_Littel28@hotmail.com", "kVkbx8w4DDc7i/YfGBAkCbs0rEoWKJf5EWQckyuLiyI=", "CL6WVH/1C2ALmS1EOwcr5bSArkqF/8z5RhV2FSjXqUs=", new DateTime(2021, 6, 12, 17, 15, 8, 296, DateTimeKind.Local).AddTicks(6570), "Kristofer.Champlin" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 17, 15, 8, 306, DateTimeKind.Local).AddTicks(1300), "DrBHVxhFmWJuTmzIQAHLMKA8bC9Ooky8dEIZOhm3s9s=", "R/qkULm19R+Rkf3TQgpho4hNlcdkKlF9E3Yp+UJ7piE=", new DateTime(2021, 6, 12, 17, 15, 8, 306, DateTimeKind.Local).AddTicks(1300) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DropColumn(
                name: "PasswordResetExpires",
                table: "Users");

            migrationBuilder.AddColumn<string>(
                name: "PasswordResetToken",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 1, 4, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(4180), false, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(4650), 4 },
                    { 18, 7, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5370), false, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5370), 9 },
                    { 17, 16, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5360), false, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5360), 9 },
                    { 15, 7, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5330), true, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5330), 11 },
                    { 14, 15, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5310), true, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5310), 18 },
                    { 13, 6, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5300), true, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5300), 6 },
                    { 12, 8, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5270), true, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5280), 18 },
                    { 11, 19, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5170), false, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5170), 5 },
                    { 19, 9, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5390), false, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5390), 3 },
                    { 10, 20, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5150), true, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5160), 21 },
                    { 8, 8, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5120), false, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5130), 16 },
                    { 7, 4, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5110), true, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5110), 20 },
                    { 6, 17, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5090), false, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5100), 4 },
                    { 5, 4, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5080), true, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5080), 8 },
                    { 4, 17, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5060), true, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5060), 1 },
                    { 3, 16, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5040), false, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5040), 5 },
                    { 2, 18, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5020), true, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5020), 21 },
                    { 9, 9, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5140), true, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5140), 11 },
                    { 20, 11, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5400), false, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5400), 3 },
                    { 16, 10, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5340), false, new DateTime(2021, 6, 12, 12, 14, 11, 169, DateTimeKind.Local).AddTicks(5340), 19 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Atque tempora cupiditate incidunt quo hic.", new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(2940), 3, new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(3420) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Et quod non minima quo ea.", new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(3810), 19, new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(3820) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 11, "Nemo est nihil et facilis.", new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(3860), new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(3860) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Molestias in officia.", new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(3890), 18, new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(3890) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Occaecati et officia neque voluptatem laboriosam repudiandae.", new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(3950), 1, new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(3960) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Vel placeat vel expedita omnis et voluptas quos repellendus.", new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4000), 7, new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4000) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Quod esse qui alias.", new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4030), 15, new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4030) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Consectetur perspiciatis est in labore voluptate.", new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4060), 5, new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4070) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Est deserunt iusto dolorem.", new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4100), 19, new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4100) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Eveniet impedit ut quia facilis.", new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4130), 2, new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4130) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Possimus quia enim rerum quia culpa ratione alias dolor cumque.", new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4180), 5, new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4180) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Ipsum sunt dolore.", new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4210), 13, new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4210) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Quisquam consectetur distinctio nesciunt ut aut neque.", new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4240), 6, new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4250) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Velit quibusdam iste.", new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4270), 17, new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4280) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Veritatis eos omnis architecto et fuga deserunt quaerat totam.", new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4310), 15, new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4320) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 1, "Est dolores maiores adipisci.", new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4340), new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4350) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Maxime explicabo sit minus vitae aut.", new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4390), 18, new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4390) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Assumenda et enim omnis quia aliquid excepturi aut consequatur perspiciatis.", new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4430), 2, new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4430) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Ea eligendi distinctio sunt rerum sapiente sequi.", new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4470), 5, new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4470) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Atque quasi sunt quisquam ipsa ea soluta veritatis est.", new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4510), 14, new DateTime(2021, 6, 12, 12, 14, 11, 162, DateTimeKind.Local).AddTicks(4510) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 931, DateTimeKind.Local).AddTicks(9050), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/983.jpg", new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2080) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2420), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/119.jpg", new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2420) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2440), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/467.jpg", new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2440) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2450), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1025.jpg", new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2460) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2470), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/945.jpg", new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2470) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2480), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/619.jpg", new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2480) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2490), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/192.jpg", new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2500) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2510), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/512.jpg", new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2510) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2520), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/69.jpg", new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2520) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2530), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/356.jpg", new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2530) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2540), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/34.jpg", new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2550) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2560), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/390.jpg", new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2560) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2570), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1144.jpg", new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2570) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2580), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/875.jpg", new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2580) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2590), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1008.jpg", new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2600) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2610), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1218.jpg", new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2610) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2620), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/884.jpg", new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2620) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2630), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1215.jpg", new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2630) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2640), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/1238.jpg", new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2650) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2660), "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/901.jpg", new DateTime(2021, 6, 12, 12, 14, 10, 932, DateTimeKind.Local).AddTicks(2660) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(4370), "https://picsum.photos/640/480/?image=834", new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(4850) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5000), "https://picsum.photos/640/480/?image=628", new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5000) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5020), "https://picsum.photos/640/480/?image=487", new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5020) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5040), "https://picsum.photos/640/480/?image=900", new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5050) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5060), "https://picsum.photos/640/480/?image=893", new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5060) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5070), "https://picsum.photos/640/480/?image=505", new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5070) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5080), "https://picsum.photos/640/480/?image=303", new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5090) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5090), "https://picsum.photos/640/480/?image=947", new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5100) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5110), "https://picsum.photos/640/480/?image=597", new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5110) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5120), "https://picsum.photos/640/480/?image=936", new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5120) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5130), "https://picsum.photos/640/480/?image=491", new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5130) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5140), "https://picsum.photos/640/480/?image=671", new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5150) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5160), "https://picsum.photos/640/480/?image=641", new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5160) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5170), "https://picsum.photos/640/480/?image=531", new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5170) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5180), "https://picsum.photos/640/480/?image=223", new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5180) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5190), "https://picsum.photos/640/480/?image=545", new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5200) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5210), "https://picsum.photos/640/480/?image=924", new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5220) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5230), "https://picsum.photos/640/480/?image=118", new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5230) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5240), "https://picsum.photos/640/480/?image=192", new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5240) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5250), "https://picsum.photos/640/480/?image=208", new DateTime(2021, 6, 12, 12, 14, 10, 934, DateTimeKind.Local).AddTicks(5250) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, new DateTime(2021, 6, 12, 12, 14, 11, 166, DateTimeKind.Local).AddTicks(380), false, 18, new DateTime(2021, 6, 12, 12, 14, 11, 166, DateTimeKind.Local).AddTicks(380), 5 },
                    { 1, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(8220), true, 13, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(8690), 12 },
                    { 19, new DateTime(2021, 6, 12, 12, 14, 11, 166, DateTimeKind.Local).AddTicks(130), false, 9, new DateTime(2021, 6, 12, 12, 14, 11, 166, DateTimeKind.Local).AddTicks(130), 20 },
                    { 18, new DateTime(2021, 6, 12, 12, 14, 11, 166, DateTimeKind.Local).AddTicks(110), false, 2, new DateTime(2021, 6, 12, 12, 14, 11, 166, DateTimeKind.Local).AddTicks(110), 17 }
                });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 17, new DateTime(2021, 6, 12, 12, 14, 11, 166, DateTimeKind.Local).AddTicks(90), false, 14, new DateTime(2021, 6, 12, 12, 14, 11, 166, DateTimeKind.Local).AddTicks(100), 7 },
                    { 16, new DateTime(2021, 6, 12, 12, 14, 11, 166, DateTimeKind.Local).AddTicks(80), false, 1, new DateTime(2021, 6, 12, 12, 14, 11, 166, DateTimeKind.Local).AddTicks(80), 20 },
                    { 14, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(9870), false, 5, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(9870), 12 },
                    { 13, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(9850), true, 19, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(9850), 4 },
                    { 12, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(9830), false, 3, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(9830), 20 },
                    { 11, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(9810), true, 7, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(9820), 5 },
                    { 15, new DateTime(2021, 6, 12, 12, 14, 11, 166, DateTimeKind.Local).AddTicks(60), true, 8, new DateTime(2021, 6, 12, 12, 14, 11, 166, DateTimeKind.Local).AddTicks(60), 7 },
                    { 9, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(9780), false, 13, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(9780), 3 },
                    { 8, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(9760), false, 16, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(9760), 3 },
                    { 7, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(9740), false, 16, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(9740), 15 },
                    { 6, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(9720), true, 2, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(9720), 21 },
                    { 10, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(9790), true, 14, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(9800), 15 },
                    { 5, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(9700), true, 7, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(9700), 15 },
                    { 4, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(9280), true, 6, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(9290), 3 },
                    { 3, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(9270), false, 17, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(9270), 19 },
                    { 2, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(9240), false, 8, new DateTime(2021, 6, 12, 12, 14, 11, 165, DateTimeKind.Local).AddTicks(9240), 1 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Harum repellendus quas non.", new DateTime(2021, 6, 12, 12, 14, 11, 158, DateTimeKind.Local).AddTicks(5250), 37, new DateTime(2021, 6, 12, 12, 14, 11, 158, DateTimeKind.Local).AddTicks(5760) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "Hic tenetur voluptate nihil.\nOptio corrupti et et tempora rerum.", new DateTime(2021, 6, 12, 12, 14, 11, 158, DateTimeKind.Local).AddTicks(8280), 22, new DateTime(2021, 6, 12, 12, 14, 11, 158, DateTimeKind.Local).AddTicks(8300) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "Pariatur sit omnis ut magni tenetur.\nEsse corrupti est omnis voluptatem reiciendis quam.", new DateTime(2021, 6, 12, 12, 14, 11, 158, DateTimeKind.Local).AddTicks(8450), 29, new DateTime(2021, 6, 12, 12, 14, 11, 158, DateTimeKind.Local).AddTicks(8460) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 2, "Non dolores delectus illum quos est voluptatem velit cupiditate.\nIpsa facere ipsum ipsam inventore a harum.\nNon sit asperiores vel doloribus eius maxime qui modi.", new DateTime(2021, 6, 12, 12, 14, 11, 158, DateTimeKind.Local).AddTicks(8570), new DateTime(2021, 6, 12, 12, 14, 11, 158, DateTimeKind.Local).AddTicks(8580) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 10, "Tempore omnis quaerat suscipit aut sint. Adipisci reiciendis possimus illo consequatur. Recusandae eum hic et praesentium est.", new DateTime(2021, 6, 12, 12, 14, 11, 158, DateTimeKind.Local).AddTicks(9480), new DateTime(2021, 6, 12, 12, 14, 11, 158, DateTimeKind.Local).AddTicks(9500) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "pariatur", new DateTime(2021, 6, 12, 12, 14, 11, 158, DateTimeKind.Local).AddTicks(9830), 32, new DateTime(2021, 6, 12, 12, 14, 11, 158, DateTimeKind.Local).AddTicks(9830) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Unde non fugit cumque animi.\nBeatae reprehenderit inventore est architecto fugit dolores perferendis.", new DateTime(2021, 6, 12, 12, 14, 11, 158, DateTimeKind.Local).AddTicks(9920), 21, new DateTime(2021, 6, 12, 12, 14, 11, 158, DateTimeKind.Local).AddTicks(9920) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, "Est consequatur possimus ad.", new DateTime(2021, 6, 12, 12, 14, 11, 158, DateTimeKind.Local).AddTicks(9960), 21, new DateTime(2021, 6, 12, 12, 14, 11, 158, DateTimeKind.Local).AddTicks(9960) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "saepe", new DateTime(2021, 6, 12, 12, 14, 11, 158, DateTimeKind.Local).AddTicks(9980), 39, new DateTime(2021, 6, 12, 12, 14, 11, 158, DateTimeKind.Local).AddTicks(9990) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "Dolores dignissimos sint est. Rerum aliquid quia aut at asperiores saepe velit iure. Et praesentium ratione laborum tempora sapiente aut voluptatem. Ad consectetur dolores est impedit nihil deserunt. Aliquid provident earum tempore in commodi.", new DateTime(2021, 6, 12, 12, 14, 11, 159, DateTimeKind.Local).AddTicks(120), 26, new DateTime(2021, 6, 12, 12, 14, 11, 159, DateTimeKind.Local).AddTicks(120) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Tenetur voluptas eum qui ut qui ad rem nisi voluptatem. Incidunt neque dolor voluptas autem culpa. Cumque et aspernatur voluptatem. Ut nam ducimus voluptatibus ipsa aspernatur debitis. Magnam doloribus alias. Dolorem necessitatibus consequatur voluptatem aliquam.", new DateTime(2021, 6, 12, 12, 14, 11, 159, DateTimeKind.Local).AddTicks(1400), 29, new DateTime(2021, 6, 12, 12, 14, 11, 159, DateTimeKind.Local).AddTicks(1410) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "qui", new DateTime(2021, 6, 12, 12, 14, 11, 159, DateTimeKind.Local).AddTicks(1450), 38, new DateTime(2021, 6, 12, 12, 14, 11, 159, DateTimeKind.Local).AddTicks(1460) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "Doloribus eligendi qui enim. Accusamus est ut voluptas nesciunt temporibus. Ut autem laudantium corporis tempora et. Accusamus quas atque non.", new DateTime(2021, 6, 12, 12, 14, 11, 159, DateTimeKind.Local).AddTicks(1560), 38, new DateTime(2021, 6, 12, 12, 14, 11, 159, DateTimeKind.Local).AddTicks(1570) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "et", new DateTime(2021, 6, 12, 12, 14, 11, 159, DateTimeKind.Local).AddTicks(1590), 21, new DateTime(2021, 6, 12, 12, 14, 11, 159, DateTimeKind.Local).AddTicks(1590) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "Atque autem odit amet error voluptatibus sint dolores.\nOfficia ab non rem quo adipisci eos.\nVoluptas rerum inventore dolorum fuga quod molestias deleniti similique.", new DateTime(2021, 6, 12, 12, 14, 11, 159, DateTimeKind.Local).AddTicks(1690), 37, new DateTime(2021, 6, 12, 12, 14, 11, 159, DateTimeKind.Local).AddTicks(1690) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "cum", new DateTime(2021, 6, 12, 12, 14, 11, 159, DateTimeKind.Local).AddTicks(1710), 38, new DateTime(2021, 6, 12, 12, 14, 11, 159, DateTimeKind.Local).AddTicks(1710) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "Dignissimos ut minima quod dolore est voluptatem magnam sint et. Velit laborum debitis. Incidunt perspiciatis doloremque.", new DateTime(2021, 6, 12, 12, 14, 11, 159, DateTimeKind.Local).AddTicks(1800), 27, new DateTime(2021, 6, 12, 12, 14, 11, 159, DateTimeKind.Local).AddTicks(1800) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 10, "Impedit asperiores nesciunt fugit magni earum id. Esse aliquid voluptates facere deserunt molestias aspernatur qui suscipit animi. Dolorem architecto voluptatum nesciunt non cupiditate.", new DateTime(2021, 6, 12, 12, 14, 11, 159, DateTimeKind.Local).AddTicks(1890), new DateTime(2021, 6, 12, 12, 14, 11, 159, DateTimeKind.Local).AddTicks(1900) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "Corrupti architecto quasi reprehenderit.", new DateTime(2021, 6, 12, 12, 14, 11, 159, DateTimeKind.Local).AddTicks(1930), 34, new DateTime(2021, 6, 12, 12, 14, 11, 159, DateTimeKind.Local).AddTicks(1930) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Saepe nemo dolor omnis qui inventore cumque.\nVelit quisquam ducimus.", new DateTime(2021, 6, 12, 12, 14, 11, 159, DateTimeKind.Local).AddTicks(1980), 33, new DateTime(2021, 6, 12, 12, 14, 11, 159, DateTimeKind.Local).AddTicks(1990) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2021, 6, 12, 12, 14, 10, 969, DateTimeKind.Local).AddTicks(6030), "Calista.Klocko72@hotmail.com", "zFQhMDTGUzv3ofp4RyRmo6xGdxgiOc8lPASYQbs0g0w=", "PORS3QPrm34Xrhft/VdNYRZoOWCQ5FwiDTTECBd/bdo=", new DateTime(2021, 6, 12, 12, 14, 10, 969, DateTimeKind.Local).AddTicks(6540), "Joshuah_Trantow" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2021, 6, 12, 12, 14, 10, 978, DateTimeKind.Local).AddTicks(9950), "Alejandrin_Runolfsson1@hotmail.com", "FyMrkhWdc3r/66TIQaN9EJt8xWv3bEBlmRqK9ROXixk=", "kEbX4iRXOW9/q9UryU2Qqd0jTLCP8L6ZVnJWMc9p4nk=", new DateTime(2021, 6, 12, 12, 14, 10, 978, DateTimeKind.Local).AddTicks(9960), "Brad_Miller20" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2021, 6, 12, 12, 14, 10, 988, DateTimeKind.Local).AddTicks(3640), "Casimir18@hotmail.com", "XY1x5uG/UqhLh6QdkbfVHkrTDdgqFW3ZBLuKr6RQGJc=", "jgJbT5YfRrJnlqxYaJmT8N/jwkkQiBPhw+wIP/ilQI8=", new DateTime(2021, 6, 12, 12, 14, 10, 988, DateTimeKind.Local).AddTicks(3650), "Christop_Rogahn" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2021, 6, 12, 12, 14, 10, 997, DateTimeKind.Local).AddTicks(5210), "Hassie78@hotmail.com", "sY73paj42DT/yDy8OwUX9kNILs1CvvVq7Ikt0ut+hZ4=", "dw0tF4XYE0JXXUAbU2bO5KIdbXCHevs/mU+K/XZwGYw=", new DateTime(2021, 6, 12, 12, 14, 10, 997, DateTimeKind.Local).AddTicks(5220), "Roman.Orn24" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2021, 6, 12, 12, 14, 11, 7, DateTimeKind.Local).AddTicks(1660), "Jonatan7@yahoo.com", "xIh810Dym9uoCU7C2JlH1UQA9SecTtd38sO7pi611jc=", "meiBG+8KGwM6szqPfwGZYsR9893lBx8D+W0clOzgcR0=", new DateTime(2021, 6, 12, 12, 14, 11, 7, DateTimeKind.Local).AddTicks(1670), "Pascale.Gaylord3" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2021, 6, 12, 12, 14, 11, 15, DateTimeKind.Local).AddTicks(9360), "Ole_Keeling@hotmail.com", "w3nSMfcBoRL0I19BFzXz1luwx1bKG89Cp5IEeZVkpHI=", "2A1RNc4OV5ALZIjdQrY8NLn0VvW0HEEiUgHgTLUzmow=", new DateTime(2021, 6, 12, 12, 14, 11, 15, DateTimeKind.Local).AddTicks(9360), "Taylor5" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2021, 6, 12, 12, 14, 11, 24, DateTimeKind.Local).AddTicks(8670), "Amari.Mitchell47@yahoo.com", "JehGhfRagY/rGTWC6zio3vhsbLU4UqDx6Anl2OXPPCE=", "JpI18kfqj+kO5KqGvK3R+WyH1v/EsVH1RFoHkjTz+6I=", new DateTime(2021, 6, 12, 12, 14, 11, 24, DateTimeKind.Local).AddTicks(8670), "Keara_Koss60" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 11, 33, DateTimeKind.Local).AddTicks(6300), "Annabelle_Gleason@hotmail.com", "HfH5QvCwHmQXY9OpNfIzXjd2205W69pUE1cOZyYma/U=", "o/DWNKbV9NGgp3nuau80kGM5vg50Q9CSmhmbQaDs3wI=", new DateTime(2021, 6, 12, 12, 14, 11, 33, DateTimeKind.Local).AddTicks(6310), "Wendy_Dicki78" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2021, 6, 12, 12, 14, 11, 42, DateTimeKind.Local).AddTicks(4380), "Freeman_McKenzie@hotmail.com", "NPhcJ9eWZ3Dy8aXr9wSzzuixHZ8bye/UsJnOtaP7VB8=", "huB6GqmRTihJY48X197+381m6bCgGGw3Bbhuc+T//9o=", new DateTime(2021, 6, 12, 12, 14, 11, 42, DateTimeKind.Local).AddTicks(4380), "Kole.Kreiger" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2021, 6, 12, 12, 14, 11, 51, DateTimeKind.Local).AddTicks(2010), "Reanna_Bergstrom@yahoo.com", "OmaSmijDq5X5Nhg4pe9SwZSagFO8f8GXk1QFCNJkB18=", "LOT4RQdjcRn7PsWzExvDPSMLta6H9ZSuLs0xmapnqmA=", new DateTime(2021, 6, 12, 12, 14, 11, 51, DateTimeKind.Local).AddTicks(2010), "Scottie75" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 11, 60, DateTimeKind.Local).AddTicks(7230), "Willow64@yahoo.com", "6TDN4mZNzt0kKUpJV8NCy6b+9oZ2RZAWGj+O3EwPgF4=", "16ekqeoCFWAbi0BCf4Ifd2eJxzIIcOVjhgjDVZzLvcI=", new DateTime(2021, 6, 12, 12, 14, 11, 60, DateTimeKind.Local).AddTicks(7250), "Allie39" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2021, 6, 12, 12, 14, 11, 69, DateTimeKind.Local).AddTicks(9620), "Dallin.Schaefer69@yahoo.com", "Wm9/tAcL8ECDaEsihzGmMgLsmXNZFCsWIrcb2+y95wQ=", "9O6JRBC+iqslSPnpu6RlyGv0lEILjUtjq36fq6/MDSo=", new DateTime(2021, 6, 12, 12, 14, 11, 69, DateTimeKind.Local).AddTicks(9620), "Tiara_Padberg66" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2021, 6, 12, 12, 14, 11, 78, DateTimeKind.Local).AddTicks(7630), "Georgette48@gmail.com", "6jSF1WCONEncjO+aVBXWgjLVQk9pY2VeEnSNJnVb/AA=", "LoPSAco7I0X2T9DT7EG5+zkIRdhOzWfqJGHgQVMpsMU=", new DateTime(2021, 6, 12, 12, 14, 11, 78, DateTimeKind.Local).AddTicks(7640), "Jovanny.Baumbach" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2021, 6, 12, 12, 14, 11, 87, DateTimeKind.Local).AddTicks(6790), "Efrain.Will@hotmail.com", "+EnsL/8s8nNPcbJ0A5M5tOV4ifazMkFE/XfXZyLJm6c=", "uUkRnIn4OkbZNt1LP4JeRMv2oybjVVo0xIZyjzzBaR8=", new DateTime(2021, 6, 12, 12, 14, 11, 87, DateTimeKind.Local).AddTicks(6800), "Ryder.Rutherford" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2021, 6, 12, 12, 14, 11, 98, DateTimeKind.Local).AddTicks(8860), "Brock_Hoeger2@gmail.com", "XK9dnbj4VF3xMo4zE3lsVPIiyYfiJ/f7Gm443e0GxIU=", "BDlm9L6ZPuS/3NK9C2iA+AH3W2t6jjrDcmY6wUgCnMQ=", new DateTime(2021, 6, 12, 12, 14, 11, 98, DateTimeKind.Local).AddTicks(8870), "Herta.OHara86" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2021, 6, 12, 12, 14, 11, 107, DateTimeKind.Local).AddTicks(7480), "Adelia.Williamson@yahoo.com", "qaBbXq5kGfx2eN9f56JufD3iOBKQjKXsL9Rep5HCoqw=", "ncr4+Vy9ZPWlD2fu8gFhcjrc55/54ElUWwxtpoSG6dc=", new DateTime(2021, 6, 12, 12, 14, 11, 107, DateTimeKind.Local).AddTicks(7480), "Wayne_Swaniawski51" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2021, 6, 12, 12, 14, 11, 116, DateTimeKind.Local).AddTicks(9250), "Amira.Pfeffer3@gmail.com", "CheUEXrmIlvCKCW4/hyUIQw72/lPa0xeFGGJqDIp9uU=", "kPBNwjDnOE3VEkpoqTYU+X5qzXtzSDzLKK80vrDD+bw=", new DateTime(2021, 6, 12, 12, 14, 11, 116, DateTimeKind.Local).AddTicks(9250), "Reed.Jakubowski65" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2021, 6, 12, 12, 14, 11, 125, DateTimeKind.Local).AddTicks(8500), "Meda.Dare@hotmail.com", "vj6XkI+iFFJer+GO03Fq2ve8zJ3gE4lgu3E4J1aTyTY=", "w6zW4eFtLAPhGxFjQxMpx6vBrEfDLINkR8xskWny/L8=", new DateTime(2021, 6, 12, 12, 14, 11, 125, DateTimeKind.Local).AddTicks(8510), "Neil66" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2021, 6, 12, 12, 14, 11, 134, DateTimeKind.Local).AddTicks(6740), "Kathryn.Kreiger@gmail.com", "YIlBbd/WwCI8XNMJTk5iW22g4/caPWTCVPG6ba09R40=", "RrXKPms3HHGZHUbDeOk83nLsK2lFfv/q8aX+Y0XuclY=", new DateTime(2021, 6, 12, 12, 14, 11, 134, DateTimeKind.Local).AddTicks(6750), "Russell_Dicki" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2021, 6, 12, 12, 14, 11, 143, DateTimeKind.Local).AddTicks(4530), "Emmett_Hayes22@yahoo.com", "GcR3PPW2YpoGb13t5dO/v41nIcOPnEwOELo1i/zSIbg=", "ckt/pOBjAB9Dd0795eVKVMwvntv0lyGCJRfZWLZmy7s=", new DateTime(2021, 6, 12, 12, 14, 11, 143, DateTimeKind.Local).AddTicks(4540), "Concepcion90" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 14, 11, 152, DateTimeKind.Local).AddTicks(4240), "pKC/kPXc9UnmpcEKBQVxS5Zut13Tb4MsNHRNmo1Rr/s=", "xw+bicdKagra0KthzB4Q82VRU79Z6MQgDfWCTyTCqww=", new DateTime(2021, 6, 12, 12, 14, 11, 152, DateTimeKind.Local).AddTicks(4240) });
        }
    }
}
