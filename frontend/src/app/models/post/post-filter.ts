export interface PostFilter {
    page: number;
    pageSize: number;
    isOnlyMine?: boolean; // null - unset, false - not mine, true - mine
    isOnlyLikedByMe?: boolean; // null, false - unset, true  - liked by me
}
