export interface UpdatePost {
    body: string;
    previewImage: string;
}
