import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { JwtInterceptor } from "./helpers/jwt.interceptor";
import { ErrorInterceptor } from "./helpers/error.interceptor";
import { RouterModule } from "@angular/router";
import { AppRoutes } from "./app.routes";
import { FormsModule } from "@angular/forms";
import { MainThreadComponent } from "./components/main-thread/main-thread.component";
import { PostComponent } from "./components/post/post.component";
import { HomeComponent } from "./components/home/home.component";
import { UserProfileComponent } from "./components/user-profile/user-profile.component";
import { AuthDialogComponent } from "./components/auth-dialog/auth-dialog.component";
import { CommentComponent } from "./components/comment/comment.component";
import { MaterialComponentsModule } from "./components/common/material-components.module";
import { PostUpdateComponent } from "./components/post-update/post-update.component";
import { PostDeleteComponent } from "./components/post-delete/post-delete.component";
import { CommentUpdateComponent } from "./components/comment-update/comment-update.component";
import { CommentDeleteComponent } from "./components/comment-delete/comment-delete.component";
import { OnePostComponent } from "./components/one-post/one-post.component";
import { PostShareDialogComponent } from "./components/post-share-dialog/post-share-dialog.component";
import { RequestPasswordResetComponent } from "./components/request-password-reset/request-password-reset.component";
import { ResetPasswordComponent } from "./components/reset-password/reset-password.component";
import { InfiniteScrollModule } from "ngx-infinite-scroll";

@NgModule({
    declarations: [
        AppComponent,
        MainThreadComponent,
        PostComponent,
        HomeComponent,
        UserProfileComponent,
        AuthDialogComponent,
        CommentComponent,
        CommentUpdateComponent,
        PostUpdateComponent,
        PostDeleteComponent,
        CommentDeleteComponent,
        OnePostComponent,
        PostShareDialogComponent,
        RequestPasswordResetComponent,
        ResetPasswordComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        MaterialComponentsModule,
        RouterModule.forRoot(AppRoutes),
        FormsModule,
        InfiniteScrollModule,
    ],
    exports: [MaterialComponentsModule],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
