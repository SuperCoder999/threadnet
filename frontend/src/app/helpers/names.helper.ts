function certainToFormattedStringWithAnd(certain: string[]): string {
    if (certain.length === 1) {
        return certain[0];
    }

    return (
        certain.slice(0, certain.length - 1).join(", ") +
        " and " +
        certain[certain.length - 1]
    );
}

export default function namesToSafeLengthString(names: string[]): string {
    if (names.length === 0) {
        return "";
    }

    const certain = names.slice(0, 20);

    if (names.length - certain.length <= 5) {
        return certainToFormattedStringWithAnd(certain);
    }

    return certain.join(", ") + " and " + (names.length - 20) + " others";
}
