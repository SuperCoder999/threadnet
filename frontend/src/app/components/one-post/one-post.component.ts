import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { Post } from "src/app/models/post/post";
import { User } from "src/app/models/user";
import { AuthenticationService } from "src/app/services/auth.service";
import { PostService } from "src/app/services/post.service";
import { SnackBarService } from "src/app/services/snack-bar.service";

@Component({
    selector: "one-post",
    templateUrl: "./one-post.component.html",
    styleUrls: ["./one-post.component.sass"],
})
export class OnePostComponent implements OnInit, OnDestroy {
    public id: number;
    public loading = true;
    public post: Post;
    public currentUser: User;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private router: Router,
        private route: ActivatedRoute,
        private postService: PostService,
        private authService: AuthenticationService,
        private snackBarService: SnackBarService
    ) {}

    public ngOnInit() {
        this.route.params
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((params) => {
                this.id = Number(params.id);
                this.fetchPost();
            });

        this.fetchUser();
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public goToAll() {
        this.router.navigate(["/"]);
    }

    private setPost(post: Post) {
        this.post = post;
        this.loading = false;
    }

    private showError(error: Error) {
        this.snackBarService.showErrorMessage(error);
        this.loading = false;
    }

    private fetchPost() {
        this.postService
            .getOnePost(this.id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((resp) => this.setPost(resp.body), this.showError);
    }

    private fetchUser() {
        this.authService
            .getUser()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (user) => (this.currentUser = user),
                () => {}
            );
    }
}
