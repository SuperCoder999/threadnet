import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnDestroy,
} from "@angular/core";

import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { PostService } from "src/app/services/post.service";
import { SnackBarService } from "src/app/services/snack-bar.service";

@Component({
    selector: "post-delete",
    templateUrl: "./post-delete.component.html",
    styleUrls: ["./post-delete.component.sass"],
})
export class PostDeleteComponent implements OnDestroy {
    @Input() public id: number;

    @Output() public onDelete: EventEmitter<number> =
        new EventEmitter<number>();

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private postService: PostService,
        private snackBarService: SnackBarService
    ) {}

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public delete() {
        this.postService
            .deletePost(this.id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                () => this.onDelete.emit(this.id),
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }
}
