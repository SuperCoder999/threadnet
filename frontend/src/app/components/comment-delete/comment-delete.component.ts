import {
    Component,
    Input,
    Output,
    EventEmitter,
    OnDestroy,
} from "@angular/core";

import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { CommentService } from "src/app/services/comment.service";
import { SnackBarService } from "src/app/services/snack-bar.service";

@Component({
    selector: "comment-delete",
    templateUrl: "./comment-delete.component.html",
    styleUrls: ["./comment-delete.component.sass"],
})
export class CommentDeleteComponent implements OnDestroy {
    @Input() public id: number;

    @Output() public onDelete: EventEmitter<number> =
        new EventEmitter<number>();

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private commentService: CommentService,
        private snackBarService: SnackBarService
    ) {}

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public delete() {
        this.commentService
            .deleteComment(this.id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                () => this.onDelete.emit(this.id),
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }
}
