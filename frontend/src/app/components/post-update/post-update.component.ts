import {
    Input,
    Component,
    OnDestroy,
    Output,
    EventEmitter,
    OnInit,
} from "@angular/core";

import { Subject } from "rxjs";
import { switchMap, takeUntil } from "rxjs/operators";
import { Post } from "src/app/models/post/post";
import { UpdatePost } from "src/app/models/post/update-post";
import { ImgurService } from "src/app/services/imgur.service";
import { PostService } from "src/app/services/post.service";
import { SnackBarService } from "src/app/services/snack-bar.service";

@Component({
    selector: "post-update",
    templateUrl: "./post-update.component.html",
    styleUrls: ["./post-update.component.sass"],
})
export class PostUpdateComponent implements OnInit, OnDestroy {
    @Input() public post: Post;
    @Output() public onCancel: EventEmitter<void> = new EventEmitter<void>();

    @Output() public onUpdate: EventEmitter<[number, Post]> = new EventEmitter<
        [number, Post]
    >();

    public newPost = {} as UpdatePost;
    public imageUrl: string;
    public imageFile: File;
    public loading: boolean = false;
    private unsubscribe$ = new Subject<void>();

    public constructor(
        private snackBarService: SnackBarService,
        private postService: PostService,
        private imgurService: ImgurService
    ) {}

    public ngOnInit() {
        this.newPost = {
            body: this.post.body,
            previewImage: this.post.previewImage,
        };

        this.imageUrl = this.post.previewImage;
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = "";
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = "";

            this.snackBarService.showErrorMessage(
                `Image can't be heavier than ~5MB`
            );

            return;
        }

        const reader = new FileReader();

        reader.addEventListener(
            "load",
            () => (this.imageUrl = reader.result as string)
        );

        reader.readAsDataURL(this.imageFile);
    }

    public removeImage() {
        this.imageUrl = undefined;
        this.imageFile = undefined;
    }

    public submit() {
        const postSubscription = !this.imageFile
            ? this.postService.updatePost(this.post.id, this.newPost)
            : this.imgurService.uploadToImgur(this.imageFile, "title").pipe(
                  switchMap((imageData) => {
                      this.post.previewImage = imageData.body.data.link;

                      return this.postService.updatePost(
                          this.post.id,
                          this.newPost
                      );
                  })
              );

        this.loading = true;

        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            (respPost) => {
                this.onUpdate.emit([this.post.id, respPost.body]);
                this.removeImage();

                this.newPost.body = this.post.body;
                this.newPost.previewImage = this.post.previewImage;
                this.loading = false;
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }

    public cancel() {
        this.onCancel.emit();
    }
}
