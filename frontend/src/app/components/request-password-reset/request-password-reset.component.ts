import { Component, OnDestroy } from "@angular/core";
import { Router } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { ResetPasswordService } from "src/app/services/reset-password.service";
import { SnackBarService } from "src/app/services/snack-bar.service";

@Component({
    selector: "request-password-reset",
    templateUrl: "./request-password-reset.component.html",
    styleUrls: ["./request-password-reset.component.sass"],
})
export class RequestPasswordResetComponent implements OnDestroy {
    public loading: boolean = false;
    public sentEmail: boolean = false;
    public emailAddress: string = "";

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private router: Router,
        private snackBarService: SnackBarService,
        private resetPasswordService: ResetPasswordService
    ) {}

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public sendEmail() {
        this.loading = true;

        this.resetPasswordService
            .requestReset(this.emailAddress)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                () => this.onSent(),
                (error) => this.onError(error)
            );
    }

    public toMain() {
        this.router.navigate(["/"]);
    }

    private onSent() {
        this.loading = false;
        this.sentEmail = true;
    }

    private onError(error: Error) {
        this.loading = false;
        this.snackBarService.showErrorMessage(error);
    }
}
