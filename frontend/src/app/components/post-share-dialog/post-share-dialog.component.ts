import { Component, Inject, OnDestroy, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { PostService } from "src/app/services/post.service";
import { SnackBarService } from "src/app/services/snack-bar.service";

@Component({
    selector: "post-share-dialog",
    templateUrl: "./post-share-dialog.component.html",
    styleUrls: ["./post-share-dialog.component.sass"],
})
export class PostShareDialogComponent implements OnInit, OnDestroy {
    public id: number;
    public link: string;
    public loading = true;
    public sharingByEmail = false;
    public shareEmail = "";

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private dialogRef: MatDialogRef<PostShareDialogComponent>,
        private postService: PostService,
        private snackBarService: SnackBarService,
        @Inject(MAT_DIALOG_DATA) private data: { id: number }
    ) {
        this.id = this.data.id;
    }

    public ngOnInit() {
        this.postService
            .getLink(this.id)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((resp) => this.setLink(resp.body.link));
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public copy(input: HTMLInputElement) {
        input.select();
        document.execCommand("copy");
        input.setSelectionRange(0, 0);

        this.snackBarService.showUsualMessage("Link has been copied!");
    }

    public openShare() {
        this.sharingByEmail = true;
        this.shareEmail = "";
    }

    public confirmShare() {
        this.loading = true;

        this.postService
            .sharePostByEmail(this.id, this.shareEmail)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(() => this.completeShare(), this.showError);
    }

    public close() {
        this.dialogRef.close();
    }

    private setLink(link: string) {
        this.link = link;
        this.loading = false;
    }

    private completeShare() {
        this.sharingByEmail = false;
        this.loading = false;
    }

    private showError(error: Error) {
        this.loading = false;
        this.snackBarService.showErrorMessage(error);
    }
}
