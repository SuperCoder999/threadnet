import {
    Component,
    EventEmitter,
    Input,
    OnDestroy,
    OnInit,
    Output,
    DoCheck,
} from "@angular/core";

import { empty, Observable, Subject } from "rxjs";
import { Post } from "../../models/post/post";
import { AuthenticationService } from "../../services/auth.service";
import { AuthDialogService } from "../../services/auth-dialog.service";
import { PostShareDialogService } from "../../services/post-share-dialog.service";
import { DialogType } from "../../models/common/auth-dialog-type";
import { PostReactionService } from "../../services/post-reaction.service";
import { NewComment } from "../../models/comment/new-comment";
import { CommentService } from "../../services/comment.service";
import { User } from "../../models/user";
import { Comment } from "../../models/comment/comment";
import { catchError, switchMap, takeUntil } from "rxjs/operators";
import { SnackBarService } from "../../services/snack-bar.service";
import namesToSafeLengthString from "src/app/helpers/names.helper";
import _ from "lodash";

@Component({
    selector: "app-post",
    templateUrl: "./post.component.html",
    styleUrls: ["./post.component.sass"],
})
export class PostComponent implements OnDestroy, OnInit, DoCheck {
    @Input("post") public propsPost: Post;
    @Input() public currentUser: User;
    @Input() public showComments: boolean;
    @Input() public showUpdateForm: boolean;

    @Output() public onDelete: EventEmitter<number> =
        new EventEmitter<number>();

    @Output() public onCommentsVisibilityChange: EventEmitter<void> =
        new EventEmitter<void>();

    @Output() public onEditFormVisibilityChange: EventEmitter<void> =
        new EventEmitter<void>();

    public newComment = {} as NewComment;
    public likerNames: string = "";
    public post: Post;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private shareDialogService: PostShareDialogService,
        private reactionService: PostReactionService,
        private commentService: CommentService,
        private snackBarService: SnackBarService
    ) {}

    public ngOnInit() {
        this.post = this.propsPost;
        this.initLikerNames();
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public ngDoCheck() {
        const result = !_.isEqual(this.propsPost, this.post);

        if (result) {
            this.post = { ...this.propsPost };
        }

        return result;
    }

    public toggleComments() {
        this.justToggleComments();
    }

    public likePost() {
        return this.reactPost();
    }

    public dislikePost() {
        return this.reactPost(false);
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        const newComments = this.sortCommentArray(
                            this.post.comments.concat(resp.body)
                        );

                        this.post.comments = newComments;
                        this.newComment.body = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public openShareDialog() {
        this.shareDialogService.openShareDialog(this.post.id);
    }

    public countLikes() {
        return this.post.reactions.filter((react) => react.isLike).length;
    }

    public countDislikes() {
        return this.post.reactions.filter((react) => !react.isLike).length;
    }

    public toggleUpdateForm() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.justToggleUpdateForm();
                    }
                });

            return;
        }

        this.justToggleUpdateForm();
    }

    public onUpdated(_id: number, newPost: Post) {
        this.post = newPost;
        this.toggleUpdateForm();
    }

    public onUpdateCancel() {
        this.toggleUpdateForm();
    }

    public onDeleted(id: number) {
        this.onDelete.emit(id);
    }

    public onCommentDelete(id: number) {
        const index = this.post.comments.findIndex((c) => c.id === id);

        if (index < 0) {
            return;
        }

        const newComments = [...this.post.comments];
        newComments.splice(index, 1);
        this.post.comments = newComments;
    }

    private reactPost(isLike: boolean = true) {
        const methodName: "likePost" | "dislikePost" = isLike
            ? "likePost"
            : "dislikePost";

        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) =>
                        this.reactionService[methodName](this.post, userResp)
                    ),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe(this.setPost);

            return;
        }

        this.reactionService[methodName](this.post, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(this.setPost);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort(
            (a, b) => +new Date(b.createdAt) - +new Date(a.createdAt)
        );
    }

    private justToggleComments() {
        this.onCommentsVisibilityChange.emit();
        this.showComments = !this.showComments;

        if (this.showComments) {
            this.showUpdateForm = false;
        }
    }

    private justToggleUpdateForm() {
        this.onEditFormVisibilityChange.emit();
        this.showUpdateForm = !this.showUpdateForm;

        if (this.showUpdateForm) {
            this.showComments = false;
        }
    }

    private initLikerNames() {
        this.likerNames = namesToSafeLengthString(
            this.post.reactions
                .filter((r) => r.isLike)
                .map((r) => r.user.userName)
        );
    }

    private setPost = (newPost: Post) => {
        this.post = newPost;
        this.initLikerNames();
    };
}
