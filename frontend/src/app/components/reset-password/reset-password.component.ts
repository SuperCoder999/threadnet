import { Component, OnDestroy, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { ResetPasswordService } from "src/app/services/reset-password.service";
import { SnackBarService } from "src/app/services/snack-bar.service";

@Component({
    selector: "request-password-reset",
    templateUrl: "./reset-password.component.html",
    styleUrls: ["./reset-password.component.sass"],
})
export class ResetPasswordComponent implements OnInit, OnDestroy {
    public loading: boolean = true;
    public wasReset: boolean = false;
    public token: string = "";
    public newPassword: string = "";

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private router: Router,
        private route: ActivatedRoute,
        private snackBarService: SnackBarService,
        private resetPasswordService: ResetPasswordService
    ) {}

    public ngOnInit() {
        this.route.params
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((params) => {
                this.token = params["token"];
                this.checkToken();
            });
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public resetPassword() {
        if (!this.wasReset) {
            this.loading = true;

            this.resetPasswordService
                .reset(this.token, this.newPassword)
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe(
                    () => this.onResetPassword(),
                    (error) => this.onError(error)
                );
        }
    }

    public toMain() {
        this.router.navigate(["/"]);
    }

    private checkToken() {
        this.resetPasswordService
            .checkToken(this.token)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                () => this.onCheckedToken(),
                (error) => this.onCheckTokenError(error)
            );
    }

    private onCheckedToken() {
        this.loading = false;
    }

    private onResetPassword() {
        this.loading = false;
        this.wasReset = true;
        this.snackBarService.showUsualMessage("Your password was reset!");
    }

    private onError(error: Error, loading: boolean = false) {
        this.loading = loading;
        this.snackBarService.showErrorMessage(error);
    }

    private onCheckTokenError(error: Error) {
        this.onError(error, true);
        this.delayedRedirect();
    }

    private delayedRedirect() {
        setInterval(this.toMain, 4000);
    }
}
