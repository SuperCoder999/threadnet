import { Component, OnInit, OnDestroy } from "@angular/core";
import { Post } from "../../models/post/post";
import { User } from "../../models/user";
import { Subject } from "rxjs";
import { MatSlideToggleChange } from "@angular/material/slide-toggle";
import { AuthenticationService } from "../../services/auth.service";
import { PostService } from "../../services/post.service";
import { AuthDialogService } from "../../services/auth-dialog.service";
import { DialogType } from "../../models/common/auth-dialog-type";
import { EventService } from "../../services/event.service";
import { ImgurService } from "../../services/imgur.service";
import { NewPost } from "../../models/post/new-post";
import { switchMap, takeUntil } from "rxjs/operators";
import { HubConnection, HubConnectionBuilder } from "@microsoft/signalr";
import { SnackBarService } from "../../services/snack-bar.service";
import { environment } from "src/environments/environment";
import { Comment } from "src/app/models/comment/comment";
import { PostFilter } from "src/app/models/post/post-filter";

@Component({
    selector: "app-main-thread",
    templateUrl: "./main-thread.component.html",
    styleUrls: ["./main-thread.component.sass"],
})
export class MainThreadComponent implements OnInit, OnDestroy {
    public posts: Post[] = [];
    public cachedPosts: Post[] = [];

    public filter: PostFilter = {
        page: 1,
        pageSize: 10,
    };

    public currentUser: User;
    public imageUrl: string;
    public imageFile: File;
    public post = {} as NewPost;
    public showPostContainer = false;
    public loading = false;
    public loadingPosts = false;

    public commentsVisibilityMap: Record<number, boolean> = {};
    public editFormVisibilityMap: Record<number, boolean> = {};

    public postHub: HubConnection;
    public commentHub: HubConnection;

    private cachedFilter: PostFilter = { ...this.filter };
    private unsubscribe$ = new Subject<void>();

    public constructor(
        private snackBarService: SnackBarService,
        private authService: AuthenticationService,
        private postService: PostService,
        private imgurService: ImgurService,
        private authDialogService: AuthDialogService,
        private eventService: EventService
    ) {}

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
        this.postHub.stop();
        this.commentHub.stop();
    }

    public ngOnInit() {
        this.registerHubs();
        this.fetchPosts();
        this.fetchUser();

        this.eventService.userChangedEvent$
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((user) => {
                this.currentUser = user;
                this.post.authorId = this.currentUser
                    ? this.currentUser.id
                    : undefined;
            });
    }

    public sendPost() {
        const postSubscription = !this.imageFile
            ? this.postService.createPost(this.post)
            : this.imgurService.uploadToImgur(this.imageFile, "title").pipe(
                  switchMap((imageData) => {
                      this.post.previewImage = imageData.body.data.link;
                      return this.postService.createPost(this.post);
                  })
              );

        this.loading = true;

        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            (respPost) => {
                this.addNewPost(respPost.body);
                this.removeImage();
                this.post.body = undefined;
                this.post.previewImage = undefined;
                this.loading = false;
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }

    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = "";
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = "";

            this.snackBarService.showErrorMessage(
                `Image can't be heavier than ~5MB`
            );

            return;
        }

        const reader = new FileReader();

        reader.addEventListener(
            "load",
            () => (this.imageUrl = reader.result as string)
        );

        reader.readAsDataURL(this.imageFile);
    }

    public removeImage() {
        this.imageUrl = undefined;
        this.imageFile = undefined;
    }

    public mineSliderChanged(event: MatSlideToggleChange) {
        const value = event.checked ? true : null;
        this.filter.isOnlyMine = value;
        this.fetchPosts();
    }

    public notMineSliderChanged(event: MatSlideToggleChange) {
        const value = event.checked ? false : null;
        this.filter.isOnlyMine = value;
        this.fetchPosts();
    }

    public likedByMeSliderChanged(event: MatSlideToggleChange) {
        this.filter.isOnlyLikedByMe = event.checked;
        this.fetchPosts();
    }

    public toggleNewPostContainer() {
        this.showPostContainer = !this.showPostContainer;
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public addNewPost(newPost: Post) {
        if (!this.cachedPosts.some((x) => x.id === newPost.id)) {
            this.cachedPosts = this.sortPostArray(
                this.cachedPosts.concat(newPost)
            );

            if (
                !this.filter.isOnlyMine ||
                (this.filter.isOnlyMine &&
                    newPost.author.id === this.currentUser.id)
            ) {
                this.posts = this.sortPostArray(this.posts.concat(newPost));
            }

            this.posts = this.getFilteredPosts();
        }
    }

    private updatePost(id: number, updater: (oldPost: Post) => Post) {
        const newPosts = [...this.cachedPosts];
        const index = newPosts.findIndex((p) => p.id === id);

        if (index < 0) {
            return;
        }

        const data = updater(newPosts[index]);

        if (!data) {
            return;
        }

        newPosts[index] = data;
        this.cachedPosts = newPosts;
        this.posts = this.getFilteredPosts();
    }

    public removePost(id: number) {
        const newPosts = [...this.cachedPosts];
        const index = this.posts.findIndex((p) => p.id === id);

        if (index < 0) {
            return;
        }

        newPosts.splice(index, 1);
        this.cachedPosts = newPosts;
        this.posts = this.getFilteredPosts();
    }

    public toggleComments(id: number) {
        const value = this.commentsVisibilityMap[id] ?? false;
        this.commentsVisibilityMap[id] = !value;

        if (!value) {
            this.editFormVisibilityMap[id] = false;
        }
    }

    public toggleEditForm(id: number) {
        const value = this.editFormVisibilityMap[id] ?? false;
        this.editFormVisibilityMap[id] = !value;

        if (!value) {
            this.commentsVisibilityMap[id] = false;
        }
    }

    public fetchMorePosts() {
        this.filter.page++;
        this.fetchPosts();
    }

    private registerHubs() {
        this.registerPostHub();
        this.registerCommentHub();
    }

    private registerPostHub() {
        this.postHub = new HubConnectionBuilder()
            .withUrl(`${environment.apiUrl}/notifications/post`)
            .build();

        this.postHub
            .start()
            .catch((error) => this.snackBarService.showErrorMessage(error));

        this.postHub.on("NewPost", (newPost: Post) => {
            if (newPost) {
                this.addNewPost(newPost);
            }
        });

        this.postHub.on("UpdatedPost", (id: number, newPost: Post) => {
            if (newPost) {
                this.updatePost(id, () => newPost);
            }
        });

        this.postHub.on("DeletedPost", (id: number) => {
            this.removePost(id);
        });

        this.postHub.on("LikedPost", (userId: number) => {
            if (this.currentUser?.id === userId) {
                this.snackBarService.showUsualMessage("Your post was liked!");
            }
        });
    }

    private registerCommentHub() {
        this.commentHub = new HubConnectionBuilder()
            .withUrl(`${environment.apiUrl}/notifications/comment`)
            .build();

        this.commentHub
            .start()
            .catch((error) => this.snackBarService.showErrorMessage(error));

        this.commentHub.on(
            "NewComment",
            (postId: number, newComment: Comment) => {
                if (newComment) {
                    this.addNewComment(postId, newComment);
                }
            }
        );

        this.commentHub.on(
            "UpdatedComment",
            (postId: number, id: number, newComment: Post) => {
                if (newComment) {
                    this.updateComment(postId, id, () => newComment);
                }
            }
        );

        this.commentHub.on("DeletedComment", (postId: number, id: number) => {
            this.removeComment(postId, id);
        });
    }

    private getFilteredPosts(): Post[] {
        let result = [...this.cachedPosts];

        if (this.filter.isOnlyMine) {
            result = result.filter((p) => p.author.id === this.currentUser.id);
        } else if (this.filter.isOnlyMine === false) {
            result = result.filter((p) => p.author.id !== this.currentUser.id);
        }

        if (this.filter.isOnlyLikedByMe) {
            result = result.filter((p) =>
                p.reactions.find(
                    (r) => r.isLike && r.user.id === this.currentUser.id
                )
            );
        }

        return this.sortPostArray(result);
    }

    private updateComment(
        postId: number,
        id: number,
        updater: (oldComment: Comment) => Comment
    ) {
        this.updatePost(postId, (oldPost) => {
            const newComments = [...oldPost.comments];
            const index = newComments.findIndex((c) => c.id === id);

            if (index < 0) {
                return;
            }

            const data = updater(newComments[index]);

            if (!data) {
                return;
            }

            newComments[index] = data;

            return {
                ...oldPost,
                comments: newComments,
            };
        });
    }

    private removeComment(postId: number, id: number) {
        this.updatePost(postId, (oldPost) => {
            const newComments = [...oldPost.comments];
            const index = newComments.findIndex((c) => c.id === id);

            if (index < 0) {
                return;
            }

            newComments.splice(index, 1);

            return {
                ...oldPost,
                comments: newComments,
            };
        });
    }

    private addNewComment(postId: number, comment: Comment) {
        this.updatePost(postId, (oldPost) => {
            const newComments = [...oldPost.comments, comment];

            return {
                ...oldPost,
                comments: newComments,
            };
        });
    }

    private fetchUser() {
        this.authService
            .getUser()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((user) => (this.currentUser = user));
    }

    private fetchPosts() {
        this.loadingPosts = true;

        this.postService
            .getPosts(this.filter)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    this.loadingPosts = false;

                    if (
                        this.filter.isOnlyMine !==
                            this.cachedFilter.isOnlyMine ||
                        this.filter.isOnlyLikedByMe !==
                            this.cachedFilter.isOnlyLikedByMe
                    ) {
                        this.filter.page = 1;
                        this.cachedFilter = { ...this.filter };
                        this.cachedPosts = resp.body;
                    } else {
                        this.cachedPosts = this.cachedPosts.concat(resp.body);
                    }

                    this.posts = this.cachedPosts;
                },
                (error) => (this.loadingPosts = false)
            );
    }

    private sortPostArray(array: Post[]): Post[] {
        return array.sort(
            (a, b) => +new Date(b.createdAt) - +new Date(a.createdAt)
        );
    }
}
