import {
    Component,
    EventEmitter,
    Input,
    OnDestroy,
    OnInit,
    Output,
} from "@angular/core";

import { empty, Subject, Observable } from "rxjs";
import { catchError, switchMap, takeUntil } from "rxjs/operators";
import namesToSafeLengthString from "src/app/helpers/names.helper";
import { User } from "src/app/models/user";
import { AuthDialogService } from "src/app/services/auth-dialog.service";
import { AuthenticationService } from "src/app/services/auth.service";
import { CommentReactionService } from "src/app/services/comment-reaction.service";
import { Comment } from "../../models/comment/comment";
import { DialogType } from "../../models/common/auth-dialog-type";

@Component({
    selector: "app-comment",
    templateUrl: "./comment.component.html",
    styleUrls: ["./comment.component.sass"],
})
export class CommentComponent implements OnDestroy, OnInit {
    @Input() public comment: Comment;
    @Input() public currentUser: User;

    @Output() public onDelete: EventEmitter<number> =
        new EventEmitter<number>();

    public showUpdateForm: boolean = false;
    public likerNames: string = "";

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private reactionService: CommentReactionService
    ) {}

    public ngOnInit() {
        this.initLikerNames();
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public toggleUpdateForm() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.justToggleUpdateForm();
                    }
                });

            return;
        }

        this.justToggleUpdateForm();
    }

    public onUpdated(_id: number, newComment: Comment) {
        this.comment = newComment;
        this.toggleUpdateForm();
    }

    public onUpdateCancel() {
        this.toggleUpdateForm();
    }

    public onDeleted(id: number) {
        this.onDelete.emit(id);
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public countLikes() {
        return this.comment.reactions.filter((react) => react.isLike).length;
    }

    public countDislikes() {
        return this.comment.reactions.filter((react) => !react.isLike).length;
    }

    public like() {
        return this.reactComment();
    }

    public dislike() {
        return this.reactComment(false);
    }

    private reactComment(isLike: boolean = true) {
        const methodName: "likeComment" | "dislikeComment" = isLike
            ? "likeComment"
            : "dislikeComment";

        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) =>
                        this.reactionService[methodName](this.comment, userResp)
                    ),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe(this.setComment);

            return;
        }

        this.reactionService[methodName](this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(this.setComment);
    }

    private initLikerNames() {
        this.likerNames = namesToSafeLengthString(
            this.comment.reactions
                .filter((r) => r.isLike)
                .map((r) => r.user.userName)
        );
    }

    private setComment = (newComment: Comment) => {
        this.comment = newComment;
        this.initLikerNames();
    };

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private justToggleUpdateForm() {
        this.showUpdateForm = !this.showUpdateForm;
    }
}
