import {
    Input,
    Component,
    OnDestroy,
    Output,
    EventEmitter,
    OnInit,
} from "@angular/core";

import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { Comment } from "src/app/models/comment/comment";
import { UpdateComment } from "src/app/models/comment/update-comment";
import { CommentService } from "src/app/services/comment.service";
import { SnackBarService } from "src/app/services/snack-bar.service";

@Component({
    selector: "comment-update",
    templateUrl: "./comment-update.component.html",
    styleUrls: ["./comment-update.component.sass"],
})
export class CommentUpdateComponent implements OnInit, OnDestroy {
    @Input() public comment: Comment;
    @Output() public onCancel: EventEmitter<void> = new EventEmitter<void>();

    @Output() public onUpdate: EventEmitter<[number, Comment]> =
        new EventEmitter<[number, Comment]>();

    public newComment = {} as UpdateComment;
    public loading: boolean = false;
    private unsubscribe$ = new Subject<void>();

    public constructor(
        private snackBarService: SnackBarService,
        private commentService: CommentService
    ) {}

    public ngOnInit() {
        this.newComment = {
            body: this.comment.body,
        };
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public submit() {
        const commentSubscribtion = this.commentService.updateComment(
            this.comment.id,
            this.newComment
        );
        this.loading = true;

        commentSubscribtion.pipe(takeUntil(this.unsubscribe$)).subscribe(
            (respPost) => {
                this.onUpdate.emit([this.comment.id, respPost.body]);

                this.newComment.body = this.comment.body;
                this.loading = false;
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }

    public cancel() {
        this.onCancel.emit();
    }
}
