import { Injectable } from "@angular/core";
import { HttpInternalService } from "./http-internal.service";

@Injectable({ providedIn: "root" })
export class ResetPasswordService {
    public routePrefix = "/api/reset-password";

    constructor(private httpService: HttpInternalService) {}

    public requestReset(email: string) {
        return this.httpService.postFullRequest(`${this.routePrefix}/request`, {
            email,
        });
    }

    public checkToken(token: string) {
        return this.httpService.postFullRequest(`${this.routePrefix}/check`, {
            token,
        });
    }

    public reset(token: string, password: string) {
        return this.httpService.putFullRequest(`${this.routePrefix}`, {
            token,
            password,
        });
    }
}
