import { Injectable } from "@angular/core";
import { HttpInternalService } from "./http-internal.service";
import { Post } from "../models/post/post";
import { NewReaction } from "../models/reactions/newReaction";
import { NewPost } from "../models/post/new-post";
import { UpdatePost } from "../models/post/update-post";
import { PostLink } from "../models/post/post-link";
import { PostFilter } from "../models/post/post-filter";

@Injectable({ providedIn: "root" })
export class PostService {
    public routePrefix = "/api/posts";

    constructor(private httpService: HttpInternalService) {}

    public getPosts(filter: PostFilter) {
        return this.httpService.postFullRequest<Post[]>(
            `${this.routePrefix}/paginated`,
            filter
        );
    }

    public getOnePost(id: number) {
        return this.httpService.getFullRequest<Post>(
            `${this.routePrefix}/${id}`
        );
    }

    public getLink(id: number) {
        return this.httpService.getFullRequest<PostLink>(
            `${this.routePrefix}/link/${id}`
        );
    }

    public createPost(post: NewPost) {
        return this.httpService.postFullRequest<Post>(
            `${this.routePrefix}`,
            post
        );
    }

    public sharePostByEmail(id: number, email: string) {
        return this.httpService.postFullRequest<Post>(
            `${this.routePrefix}/share/${id}`,
            { email }
        );
    }

    public updatePost(id: number, data: UpdatePost) {
        return this.httpService.putFullRequest<Post>(
            `${this.routePrefix}/${id}`,
            data
        );
    }

    public reactPost(reaction: NewReaction) {
        return this.httpService.postFullRequest<Post>(
            `${this.routePrefix}/react`,
            reaction
        );
    }

    public deletePost(id: number) {
        return this.httpService.deleteRequest<void>(
            `${this.routePrefix}/${id}`
        );
    }
}
