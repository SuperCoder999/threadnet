import { Injectable } from "@angular/core";
import { Comment } from "../models/comment/comment";
import { NewReaction } from "../models/reactions/newReaction";
import { CommentService } from "./comment.service";
import { User } from "../models/user";
import { map, catchError } from "rxjs/operators";
import { of } from "rxjs";
import { Reaction } from "../models/reactions/reaction";

@Injectable({ providedIn: "root" })
export class CommentReactionService {
    public constructor(private commentService: CommentService) {}

    public likeComment(comment: Comment, currentUser: User) {
        return this.reactComment(comment, currentUser);
    }

    public dislikeComment(comment: Comment, currentUser: User) {
        return this.reactComment(comment, currentUser, false);
    }

    private reactComment(
        comment: Comment,
        currentUser: User,
        isLike: boolean = true
    ) {
        const innerComment = comment;

        const reaction: NewReaction = {
            entityId: innerComment.id,
            isLike,
            userId: currentUser.id,
        };

        let currentReaction: Reaction | null = innerComment.reactions.find(
            (r) => r.user.id === currentUser.id
        );

        let newReactions = [...innerComment.reactions];
        let oldReactions = [...innerComment.reactions];

        if (currentReaction) {
            if (currentReaction.isLike === isLike) {
                newReactions = newReactions.filter(
                    (x) => x.user.id !== currentUser.id
                );
            } else {
                const index = newReactions.findIndex(
                    (x) => x.user.id === currentUser.id
                );

                if (index > -1) {
                    newReactions[index].isLike = !newReactions[index].isLike;
                }
            }
        } else {
            newReactions = newReactions.concat({
                isLike,
                user: currentUser,
            });
        }

        innerComment.reactions = newReactions;

        currentReaction = newReactions.find(
            (x) => x.user.id === currentUser.id
        );

        return this.commentService.reactComment(reaction).pipe(
            map(() => innerComment),
            catchError(() => {
                // revert current array changes in case of any error
                innerComment.reactions = oldReactions;

                return of(innerComment);
            })
        );
    }
}
