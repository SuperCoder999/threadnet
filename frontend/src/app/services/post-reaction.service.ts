import { Injectable } from "@angular/core";
import { Post } from "../models/post/post";
import { NewReaction } from "../models/reactions/newReaction";
import { PostService } from "./post.service";
import { User } from "../models/user";
import { map, catchError } from "rxjs/operators";
import { of } from "rxjs";
import { Reaction } from "../models/reactions/reaction";

@Injectable({ providedIn: "root" })
export class PostReactionService {
    public constructor(private postService: PostService) {}

    public likePost(post: Post, currentUser: User) {
        return this.reactPost(post, currentUser);
    }

    public dislikePost(post: Post, currentUser: User) {
        return this.reactPost(post, currentUser, false);
    }

    private reactPost(post: Post, currentUser: User, isLike: boolean = true) {
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike,
            userId: currentUser.id,
        };

        let currentReaction: Reaction | null = innerPost.reactions.find(
            (r) => r.user.id === currentUser.id
        );

        let newReactions = [...innerPost.reactions];
        let oldReactions = [...innerPost.reactions];

        if (currentReaction) {
            if (currentReaction.isLike === isLike) {
                newReactions = newReactions.filter(
                    (x) => x.user.id !== currentUser.id
                );
            } else {
                const index = newReactions.findIndex(
                    (x) => x.user.id === currentUser.id
                );

                if (index > -1) {
                    newReactions[index].isLike = !newReactions[index].isLike;
                }
            }
        } else {
            newReactions = newReactions.concat({
                isLike,
                user: currentUser,
            });
        }

        innerPost.reactions = newReactions;

        currentReaction = newReactions.find(
            (x) => x.user.id === currentUser.id
        );

        return this.postService.reactPost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                // revert current array changes in case of any error
                innerPost.reactions = oldReactions;

                return of(innerPost);
            })
        );
    }
}
