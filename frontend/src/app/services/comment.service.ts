import { Injectable } from "@angular/core";
import { HttpInternalService } from "./http-internal.service";
import { NewComment } from "../models/comment/new-comment";
import { UpdateComment } from "../models/comment/update-comment";
import { Comment } from "../models/comment/comment";
import { NewReaction } from "../models/reactions/newReaction";

@Injectable({ providedIn: "root" })
export class CommentService {
    public routePrefix = "/api/comments";

    constructor(private httpService: HttpInternalService) {}

    public createComment(post: NewComment) {
        return this.httpService.postFullRequest<Comment>(
            `${this.routePrefix}`,
            post
        );
    }

    public reactComment(reaction: NewReaction) {
        return this.httpService.postFullRequest<Comment>(
            `${this.routePrefix}/react`,
            reaction
        );
    }

    public updateComment(id: number, data: UpdateComment) {
        return this.httpService.putFullRequest<Comment>(
            `${this.routePrefix}/${id}`,
            data
        );
    }

    public deleteComment(id: number) {
        return this.httpService.deleteRequest<void>(
            `${this.routePrefix}/${id}`
        );
    }
}
