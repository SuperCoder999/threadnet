import { Injectable } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { PostShareDialogComponent } from "../components/post-share-dialog/post-share-dialog.component";

@Injectable({ providedIn: "root" })
export class PostShareDialogService {
    public constructor(private dialog: MatDialog) {}

    public openShareDialog(postId: number) {
        this.dialog.open(PostShareDialogComponent, {
            data: { id: postId },
            minWidth: 500,
            autoFocus: true,
            backdropClass: "dialog-backdrop",
            position: {
                top: "20vh",
            },
        });
    }
}
