# Thread .NET - mini-project Thread

**See Trello board [here](https://trello.com/b/ePv7Tviv/thread-net-bsa-2021-mini-project)**

## Technologies:

### Backend:

- [.NET 5](https://dotnet.microsoft.com/download)
- [SQL Server](https://www.microsoft.com/sql-server/sql-server-downloads)
- [EF Core](https://docs.microsoft.com/ef/core)
- [Docker](https://www.docker.com)
- [FluentValidation](https://github.com/JeremySkinner/FluentValidation)
- [AutoMapper](https://github.com/AutoMapper/AutoMapper)
- [Bogus](https://github.com/bchavez/Bogus)
- [JWT](https://jwt.io)
- [AES](https://en.wikipedia.org/wiki/AES)
- [SignalR](https://dotnet.microsoft.com/apps/aspnet/real-time)

### Frontend:

- [Angular](https://angular.io)
- [Angular Material](https://material.angular.io)

<br />

## Recommended instruments:

- [Visual Studio Code](https://code.visualstudio.com)
- [Postman](https://www.getpostman.com)

<br />

## Set up:

Make sure, that you have Docker, .NET 5 and NodeJS installed on your computer

- Clone the repository
- `cd <project-directory>`

### Backend:

- `docker-compose up -d`
- Create `.env` and `.database.env` based on `.env.example` and `.database.env.example` in the `backend` folder

### Frontend:

- `npm install`

<br />

## Build

### Backend:

- `dotnet build`

### Frontend:

- `npm run build`

<br />

## Initializing database:

- After `docker-compose up -d` in `backend` folder database will be initialized
- Then `cd "Thread .NET.DAL"` and `dotnet ef database update`

### If `dotnet ef database update` fails:

- [Install EF Core globally](https://docs.microsoft.com/en-us/ef/core/miscellaneous/cli/dotnet)
- See instructions in the `Thread .NET.DAL/Context/ThreadContext.cs` file

<br />

## Running the app:

### Backend:

- `dotnet run`
- The app will start on `http://localhost:5000` and `https://localhost:5001`

### Frontend

- `npm start`

<br />

## NOTES:

- AES is used for reset password token because:
  1. It is not authentication
  2. It has 2 keys
  3. It is HEX, so we can send it in GET request
  4. It's simplier to configure
